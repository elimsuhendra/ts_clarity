@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            @foreach($slideshow as $slide)
            <div class="swiper-slide">
                <img src="{{ asset('components/front/images/slideshow/') }}/{{ $slide->image }}" class="img-responsive cus_slide img_width" />
            </div>
            @endforeach
        </div>

        <div class="swiper-pagination"></div>
    </div>
</div>

<div class="row home_als">
    @foreach($alasan->post as $als)
        <div class="col-md-4 col-sm-4 col-xs-12 home_als_con">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <img src="{{ asset('components/front/images/post/') }}/{{ $als->id }}/{{ $als->thumbnail }}" class="img-responsive img_center" />
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="align_center2">
                        <div class="home_als_tl">{{ $als->post_name }}</div>
                        <div class="home_als_des">{!! $als->content !!}</div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

@if($category != "")
<div class="row home_cat_con">
    <div class="container">
        <div class="row">
            <div id="home_cat_tl">{{ $category->category_name }}</div>
            <div id="home_cat_desc">{{ $category->description }}</div>
        </div>

        <div class="row">
            @foreach($sub_category as $sub_cat)
            <div class="col-md-4 col-sm-4 col-xs-12 home_cat_cons">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 home_cat_img">
                        <img src="{{ asset('components/front/images/product_category') }}/{{ $sub_cat->image }}" class="img-responsive img_center img_width" />
                        
                        <div class="home_cat_button">
                            <a href="#">
                                <button class="btn"><b>BELI SEKARANG</b></button>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 home_subcat_tl">{{ $sub_cat->category_name }}</div>
                    <div class="col-md-12 col-sm-12 col-xs-12 home_subcat_des">{{ $sub_cat->description }}</div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif

@if($product_terbaru != "")
<div class="row home_terbaru">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 home_terbaru_tl">
                <span>Produk Terbaru</span>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                @php 
                    $tbr = 0;
                    $count_tbr = count($product_terbaru);
                @endphp

                @foreach($product_terbaru as $terb => $prd_tbr)
                    @if($tbr == 0)
                        <div class="row">
                    @endif

                    <div class="col-md-3 col-sm-6 col-xs-12 home_product_con">
                        @foreach($prd_tbr->attr_id as $prd_at)
                            <input type="hidden" class="product-attr_pr_{{ $prd_tbr->id }}" data-id="{{ $prd_at }}" />
                        @endforeach

                        <input type="hidden" class="product-qty" value="1" />
                        <input type="hidden" class="weight" data-value="{{ $prd_tbr->weight }}" />
                    
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ url('/product') }}/{{ $prd_tbr->name_alias }}">
                                    <img src="{{ asset('components/front/images/product') }}/{{ $prd_tbr->id }}/{{ $prd_tbr->image }}" class="img-responsive img_center" />
                                </a>

                                @if($prd_tbr->discount_status == 'y' && $prd_tbr->discount_type == 'p')
                                <div class="home_img_disc">
                                    <div class="home_div_disc">{{ $prd_tbr->discount_amount }}%</div>
                                </div>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_name">
                                <a href="{{ url('/product') }}/{{ $prd_tbr->name_alias }}" class="name_elipse">{{ $prd_tbr->name }}</a>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_cat">
                                {{ $prd_tbr->category }}
                            </div>

                            @if($prd_tbr->discount_status == 'y')
                                <div class="col-md-12 col-sm-12 col-xs-12 home_product_disc">
                                    Rp {{ number_format($prd_tbr->price),0,',','.' }}
                                </div>

                                @php
                                    $price = $prd_tbr->discount;
                                @endphp
                            @else
                                @php
                                    $price = $prd_tbr->price;
                                @endphp

                                <div class="col-md-12 col-sm-12 col-xs-12 home_product_disc_none">
                                    &nbsp;
                                </div>
                            @endif     
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_rl">
                                <div class="row home_product_block">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="row">
                                            Rp {{ number_format($price),0,',','.' }}
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-2 home_product_icon_n">
                                        <div class="row">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <div class="col-md-12 col-sm-12 col-xs-12 home_cart_button">
                                <button class="btn add-cart-front" data-id="{{ $prd_tbr->id }}" data-name="pr">TAMBAH KE BAG</button>
                            </div>            
                        </div>
                    </div>

                    @php
                        $tbr++;
                    @endphp

                    @if($count_tbr > $lmt)
                        @php
                            $terb = $terb + 1;
                        @endphp

                        @if($count_tbr - $terb != 0)
                            @if($tbr == 4)
                                </div>

                                @php
                                    $tbr = 0;
                                @endphp
                            @endif
                        @else
                            </div>
                        @endif
                    @else
                        @if($tbr == $count_tbr)
                            </div>
                        @endif
                    @endif
                @endforeach

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 home_product_dll">
                        <div class="row">
                            <a href="{{ url('/filter/produk-terbaru') }}">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="row home_banner">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row home_banner_con">
            <img src="{{ asset('components/front/images/banner') }}/{{ $banner_2->image }}" class="img-responsive img_width" />

            <div class="home_banner_cons">
                <div class="home_banner_sub">
                    <div id="home_banner_text">
                        <p>Mau potongan harga produk Clarity setiap harinya up to 90%?</p>
                        <p>Ayo langganan katalog kami. Gratis !!!</p>
                    </div>

                    <div class="col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
                        <form method="POST" action="{{ url('/subscribe') }}">
                          {{csrf_field()}}

                          <div class="input-group home_subscribe">
                            <input type="email" class="form-control" name="subscribe" placeholder="Masukkan email anda">
                            <div class="input-group-btn">
                              <button class="btn" type="submit">
                                Subscribe
                              </button>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if($product_rekomendasi != "")
<div class="row home_rekomem">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 home_rekomen_tl">
                <span>Produk Rekomendasi</span>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                @php 
                    $rek = 0;
                    $count_rek = count($product_rekomendasi);
                @endphp

                @foreach($product_rekomendasi as $reko => $prd_rek)
                    @if($rek == 0)
                        <div class="row">
                    @endif

                    <div class="col-md-3 col-sm-6 col-xs-12 home_product_con">
                        @foreach($prd_rek->attr_id as $prd_at)
                            <input type="hidden" class="product-attr_prekom_{{ $prd_rek->id }}" data-id="{{ $prd_at }}" />
                        @endforeach

                        <input type="hidden" class="product-qty" value="1" />
                        <input type="hidden" class="weight" data-value="{{ $prd_rek->weight }}" />
                    
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ url('/product') }}/{{ $prd_rek->name_alias }}">
                                    <img src="{{ asset('components/front/images/product') }}/{{ $prd_rek->id }}/{{ $prd_rek->image }}" class="img-responsive img_center" />
                                </a>

                                @if($prd_rek->discount_status == 'y' && $prd_rek->discount_type == 'p')
                                <div class="home_img_disc">
                                    <div class="home_div_disc">{{ $prd_rek->discount_amount }}%</div>
                                </div>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_name">
                                <a href="{{ url('/product') }}/{{ $prd_rek->name_alias }}" class="name_elipse">{{ $prd_rek->name }}</a>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_cat">
                                {{ $prd_rek->category }}
                            </div>

                            @if($prd_rek->discount_status == 'y')
                                <div class="col-md-12 col-sm-12 col-xs-12 home_product_disc">
                                    Rp {{ number_format($prd_rek->price),0,',','.' }}
                                </div>

                                @php
                                    $price = $prd_rek->discount;
                                @endphp
                            @else
                                @php
                                    $price = $prd_rek->price;
                                @endphp

                                <div class="col-md-12 col-sm-12 col-xs-12 home_product_disc_none">
                                    &nbsp;
                                </div>
                            @endif     
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_rl">
                                <div class="row home_product_block">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="row">
                                            Rp {{ number_format($price),0,',','.' }}
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-2 home_product_icon_n">
                                        <div class="row">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <div class="col-md-12 col-sm-12 col-xs-12 home_cart_button">
                                <button class="btn add-cart-front" data-id="{{ $prd_rek->id }}" data-name="prekom">TAMBAH KE BAG</button>
                            </div>            
                        </div>
                    </div>

                    @php
                        $rek++;
                    @endphp

                    @if($count_rek > $lmt)
                        @php
                            $reko = $reko + 1;
                        @endphp

                        @if($count_rek - $reko != 0)
                            @if($rek == 4)
                                </div>

                                @php
                                    $rek = 0;
                                @endphp
                            @endif
                        @else
                            </div>
                        @endif
                    @else
                        @if($rek == $count_rek)
                            </div>
                        @endif
                    @endif
                @endforeach
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 home_product_dll">
                        <div class="row">
                            <a href="{{ url('/filter/produk-rekomendasi') }}">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if($promo_terbaru != "")
<div class="row home_promo">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 home_promo_tl">
                <span>Furniture Clearance</span>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                @php 
                    $prom = 0;
                    $count_prom = count($promo_terbaru);
                @endphp

                @foreach($promo_terbaru as $promo => $prd_prm)
                    @if($prom == 0)
                        <div class="row">
                    @endif

                    <div class="col-md-3 col-sm-6 col-xs-12 home_product_con">
                        @foreach($prd_prm->attr_id as $prd_at)
                            <input type="hidden" class="product-attr_prom_{{ $prd_prm->id }}" data-id="{{ $prd_at }}" />
                        @endforeach

                        <input type="hidden" class="product-qty" value="1" />
                        <input type="hidden" class="weight" data-value="{{ $prd_prm->weight }}" />
                    
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ url('/product') }}/{{ $prd_prm->name_alias }}">
                                    <img src="{{ asset('components/front/images/product') }}/{{ $prd_prm->id }}/{{ $prd_prm->image }}" class="img-responsive img_center" />
                                </a>

                                @if($prd_prm->discount_status == 'y' && $prd_prm->discount_type == 'p')
                                <div class="home_img_disc">
                                    <div class="home_div_disc">{{ $prd_prm->discount_amount }}%</div>
                                </div>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_name">
                                <a href="{{ url('/product') }}/{{ $prd_prm->name_alias }}" class="name_elipse">{{ $prd_prm->name }}</a>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_cat">
                                {{ $prd_prm->category }}
                            </div>

                            @if($prd_prm->discount_status == 'y')
                                <div class="col-md-12 col-sm-12 col-xs-12 home_product_disc">
                                    Rp {{ number_format($prd_prm->price),0,',','.' }}
                                </div>

                                @php
                                    $price = $prd_prm->discount;
                                @endphp
                            @else
                                @php
                                    $price = $prd_prm->price;
                                @endphp

                                <div class="col-md-12 col-sm-12 col-xs-12 home_product_disc_none">
                                    &nbsp;
                                </div>
                            @endif     
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 home_product_rl">
                                <div class="row home_product_block">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="row">
                                            Rp {{ number_format($price),0,',','.' }}
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-2 home_product_icon_n">
                                        <div class="row">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <div class="col-md-12 col-sm-12 col-xs-12 home_cart_button">
                                <button class="btn add-cart-front" data-id="{{ $prd_prm->id }}" data-name="prom">TAMBAH KE BAG</button>
                            </div>            
                        </div>
                    </div>

                    @php
                        $prom++;
                    @endphp

                    @if($count_prom > $lmt)
                        @php
                            $promo = $promo + 1;
                        @endphp

                        @if($count_rek - $promo != 0)
                            @if($prom == 4)
                                </div>

                                @php
                                    $prom = 0;
                                @endphp
                            @endif
                        @else
                            </div>
                        @endif
                    @else
                        @if($prom == $count_prom)
                            </div>
                        @endif
                    @endif
                @endforeach

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 home_product_dll">
                        <div class="row">
                            <a href="{{ url('/filter/promo-terbaru') }}">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@push('custom_scripts')
<script>
$(document).ready(function() {
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        effect: 'fade',
        autoplay: 2500,
        autoplayDisableOnInteraction: false,
    });

    var swiper = new Swiper('.swiper-1', {
      slidesPerView: 3,
      spaceBetween: 30,
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });

    $(document).on('click', '.ct_hm_click', function(){
        var id = $(this).data('id');
        $('#form_'+id).submit();
    });
});
</script>
@endpush
