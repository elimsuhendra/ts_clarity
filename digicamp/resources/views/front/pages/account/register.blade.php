@extends($view_path.'.layouts.master')
@section('content')
<div class="row regis_con" style="background-image:url('{{ asset('components/front/images/other/login.jpg') }}');">
    <form method="POST" action="{{ url('/do-register') }}">
    {{ csrf_field() }}

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row regis_bg flex_table">
            <div class="col-md-6 col-sm-6 col-xs-12 regis_col1">
                <div class="row align_center2">
                    <div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-12 col-xs-12 regis_cons">
                        <div id="reg_tl">Sign up Today!</div>

                        @if (count($errors) > 0)
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group cus_group">
                          <label>FULL NAME </label>
                          <input type="text" class="form-control" name="name">
                        </div>

                        <div class="form-group cus_group">
                          <label>EMAIL </label>
                          <input type="email" class="form-control" name="email">
                        </div>

                        <div class="form-group cus_group">
                          <label>PASSWORD </label>
                          <input type="password" class="form-control" name="password">
                        </div>

                        <div class="form-group cus_group">
                          <label>CONFIRM PASSWORD </label>
                          <input type="password" class="form-control" name="password_confirmation">
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="row flex_table">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <button type="submit" class="btn cus_button2">Saya Ingin Mendaftar!</button>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 reg_link">
                                    <a href="{{ url('/login') }}">Login</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12 regis_cons2">
                        <div class="row">
                            <div id="reg_tl2">Pelayanan yang terbaik untuk anda</div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 reg_txt">
                                <div class="row flex_table">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <img src="{{ asset('components/front/images/other/regis_icon1.png') }}" class="img-responsive img_center" />
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12 regis_text">
                                        <div class="row reg_text_con">
                                            <div class="col-md-12 col-sm-12 col-xs-12 reg_text1">SOLUSI PEMBAYARAN</div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 reg_text2">di Clarity terdapat kemudahan Cicilan 0% di semua Bank manapun.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 reg_txt">
                                <div class="row flex_table">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <img src="{{ asset('components/front/images/other/regis_icon2.png') }}" class="img-responsive img_center" />
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12 regis_text">
                                        <div class="row reg_text_con">
                                            <div class="col-md-12 col-sm-12 col-xs-12 reg_text1">PERAKITAN</div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 reg_text2">Clarity memberikan gratis peraktian perabotan di kantor anda</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 reg_txt">
                                <div class="row flex_table">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <img src="{{ asset('components/front/images/other/regis_icon3.png') }}" class="img-responsive img_center" />
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12 regis_text">
                                        <div class="row reg_text_con">
                                            <div class="col-md-12 col-sm-12 col-xs-12 reg_text1">GARANSI</div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 reg_text2">Clarity menawarkan garansi di berbagai produk.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 reg_link2">
                                <a href="{{ url('/') }}">{{ str_replace("http://", "", url('/')) }}</a>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 reg_copy">
                                Copyright Clarityfurniture 2017
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
@endsection