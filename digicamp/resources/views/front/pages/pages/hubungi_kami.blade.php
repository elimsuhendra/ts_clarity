@extends($view_path.'.layouts.master')
@section('content')
<div class="row cb_banner" style="">
	<div class="cb_abo_op"></div>

	<div class="cus_container">
		<div class="row cb_banner_con">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<h1 class="ks_header">HUBUNGI KAMI</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row hk">
	<div class="cus_container">
		<div class="row ct_us_cons">
			<!-- <div class="col-md-12 col-sm-12 col-xs-12 ct_us_map">
				<iframe src="{{ $gmaps }}" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div> -->

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
						<div class="hk_header">
							<b>Jika terdapat pertanyaan, kami siap membantu. Hubungi layanan pelanggan Clarity atau temukan jawabannya di bawah ini.Temukan jawaban saat ini juga:</b>
						</div>
						<br><br>
						<div class="hk_faq">
							{!! $data->content !!}
						</div>
				</div>
				<br><br>
				<div class="row hk_content_2">
					<div class="col-md-4 col-sm-4 col-xs-12 hk_email">
						<b>E-mail</b><br>
						<span style="color:blue;">{{$web_email}}</span><br>
						E-mail kami kapanpun dan kami akan membalasnya dalam 24 jam.
					</div>

					<div class="col-md-4 col-sm-4 col-xs-12 hk_telepon">
						<b>Telepon</b><br>
						<b>{{$phone}}</b><br>
						Anda dapat menghubungi kami<br>
						<b>{{$clock_1}}</b>
					</div>				
				</div>
				<br><br>
				<div class="row hk_content_3">
					<img alt="" class="img-responsive" src="{{$data->image ? asset('components/front/images/pages/'.$data->image) : ''}}" />
				</div>
				<br><br>
				<div class="row hk_content_4">
						<div class="hk_header">
							<b>Hubungi Clarity BUSINESS</b><br>
							<b>Apakah Anda pelanggan Clarity BISNIS ? </b>
						</div>
						<br><br>
						<div class="hk_faq">
							Waktu dan finansial sangat berharga - Baik dalam bidang retail, perhotelan dan pariwisata atau perkantoran. Oleh karena itu kami memastikan perabotan yang didesain khusus untuk bisnis adalah perabotan yang fungsional dengan harga terjangkau.<br><br>

							Baca lebih lengkap tentang Clarity Bisnis di sini<br>
							Anda dapat menghubungi Clarity Bisnis di +62 21 - 2985 3990:<br>
							{{$clock_1}}<br><br>

							Untuk pertanyaan lainnya silakan kirimkan e-mail ke: <span style="color:blue;">{{ $data->web_email }}<span style="color:blue;">
						</div>
				</div>
				<div class="row hk_content_3">
					<img alt="" class="img-responsive" src="{{$data->image ? asset('components/front/images/pages/'.$data->image_2) : ''}}" />
				</div>
				<br><br><br>
			</div>
		</div>
	</div>
</div>
@endsection