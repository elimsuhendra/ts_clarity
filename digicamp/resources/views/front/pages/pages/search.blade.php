@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
    <div class="cus_container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row flex_table">
                    <div class="col-md-12 col-sm-12 col-xs-12 flt_con">
                        <div class="row">
                            <h1>Search Result</h1>
                        </div>
                    </div>

                    @foreach($product as $pro)
                    <div class="col-md-12 col-sm-12 col-xs-12 flt_product_con">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ url('/product') }}/{{ $pro->name_alias }}">
                                    <img src="{{ asset('components/front/images/product') }}/{{ $pro->id }}/{{ $pro->image }}" class="img-responsive img_center" />
                                </a>

                                @if($pro->discount_status == 'y' && $pro->discount_type == 'p')
                                <div class="flt_img_disc">
                                    <div class="flt_div_disc">{{ $pro->discount_amount }}%</div>
                                </div>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 flt_product_name">
                                <a href="{{ url('/product') }}/{{ $pro->name_alias }}"><p class="name_elipse">{{ $pro->name }}</p></a>
                            </div>

                            @if($pro->discount_status == 'y')
                            <div class="col-md-12 col-sm-12 col-xs-12 flt_product_disc">
                                <p>Rp {{ number_format($pro->price),0,',','.' }}</p>
                            </div>

                                @php
                                    $price = $pro->discount;
                                @endphp
                            @else
                                @php
                                    $price = $pro->price;
                                @endphp
                            @endif    

                            <div class="col-md-12 col-sm-12 col-xs-12 flt_product_rl">
                                <p>Rp {{ number_format($price),0,',','.' }}</p>
                            </div>   
                        </div>
                    </div>
                    @endforeach

                    @if(count($product) > 0)
                        <div class="col-md-12 col-sm-12 col-xs-12 flt_pagination">
                            {{ $product->links() }}
                        </div>
                    @else
                        <div class="col-md-12 col-sm-12 col-xs-12 pro_not_found">
                            <h1>Product Tidak Ditemukan</h1>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection