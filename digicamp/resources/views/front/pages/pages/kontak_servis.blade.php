@extends($view_path.'.layouts.master')
@section('content')
<div class="row cb_banner" style="">
	<div class="cb_abo_op"></div>

	<div class="cus_container ">
		<div class="row cb_banner_con">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1 class="ks_header">KONTAK SERVIS</h1>
				<div class="ks_header_dt">
					<div class="col-md-2 col-sm-2 col-xs-2 ks_header_dt_1"></div>
					<div class="col-md-6 col-sm-10 col-xs-10 ks_header_dt_2">Selamat datang di perusahaan kami</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row hk">
	<br><br>
	<div class="row">
		<div class="row ks_content_1" style="background-image: url('{{ asset('components/front/images/banner/'.$banner->image) }}');">
			<div class="kontak_kami">
				<div class='kontak_kami_header'>
					<img src="{{ asset('components/front/images/other/icon_pointer_northeast.png') }}" class="" />
					<span class="kontak_kami_header_1">KONTAK KAMI</span>
				</div>
				<br><br><br>
				<div class='address'>
					<h4>Clarity Furniture</h4>
					
					<div class="address_dt">
						{!! $address !!}
					</div>
				</div>
			</div>
			<div class=""></div>
		</div>
	</div>
	<div class="cus_container">
		<div class="row ct_us_cons">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row ks_content_2">
					<div class="col-md-5 col-sm-5 col-xs-12 ks-form">
						<div class="ks-form-header">
							KIRIM PESANMU DISINI<br>
							KAMI DENGAN SENANG<br> 
							MEMBANTU !
						</div>
						<br><br>
						<div class="ks-form-content">
							<form action="kirim_pesan" method="POST">
						  		{{ csrf_field() }}
						  	  	  @if(session()->has('message'))
								    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									    <div class="alert alert-success alert-dismissable">
									     	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
									        {{ session()->get('message') }}
									    </div>
								    </div>
								  @endif

						  	  	  @if (count($errors) > 0)
								  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									  <div class="alert alert-danger alert-dismissable">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										        
										<ul>
										  @foreach ($errors->all() as $error)
										    <li>{{ $error }}</li>
										  @endforeach
										</ul>
									  </div>
								  	</div>
								  @endif


						  	  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ap_input_con">
						  	  	  	<div class="row">
							  	  	  	<input type="text" class="form-control form-control-cust" name="name" placeholder="Nama">
						  	  	  	</div>
						  	  	  </div>

						  	  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ap_input_con">
						  	  	  	<div class="row">
							  	  	  	<input type="text" class="form-control form-control-cust" name="email" placeholder="Email" required>
						  	  	  	</div>
						  	  	  </div>

						  	  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ap_input_con">
						  	  	  	<div class="row">
							  	  	  	<input type="text" class="form-control form-control-cust" name="no_telp" placeholder="No. Telepon" required>		  	  	  
						  	  	  	</div>
						  	  	  </div>

						  	  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ap_input_con">
						  	  	  	<div class="row">							  	  	  
							  	  	  	<textarea class="form-control form-control-cust" rows="3" name="address" placeholder="Kirim Pesan" required></textarea>
						  	  	  	</div>
						  	  	  </div>

						  	  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ap_input_con">
						  	  	  	  <div class="row row-eq-height">
						  	  	  	  	  <button type="submit" class="btn ap_submit_btn">Kirim</button>
						  	  	  	  </div>
						  	  	  </div>
					  	  	</form>
					  	</div>
					</div>
					<div class="col-md-7 col-sm-7 col-xs-12 ct_us_map">
						<iframe src="{{ $gmaps }}" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<!-- <div class="cari_lokasi">
						<div class='cari_lokasi_header'>
							<span class="cari_lokasi_header_1">Produk berkualitas dengan harga yang rendah</span>
						</div>
						<br><br><br>
						<div class='cari_lokasi_dt'>
							<h4>CARI LOKASI KAMI <br>DISINI</h4>
						</div>
					</div> -->
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 ct_us_ds_md_tl">
								<p>CUSTOMER SERVICES</p>
							</div>
						</div>

						<div class="row">
        					@foreach($ctc as $cnt)
								<div class="col-md-4 col-sm-4 col-xs-4 ct_us_ds_mds">
									<img src="{{ asset('components/front/images/contact') }}/{{ $cnt->image }}" class="img-responsive" />
									
									<p>{{ $cnt->value }}</p>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection