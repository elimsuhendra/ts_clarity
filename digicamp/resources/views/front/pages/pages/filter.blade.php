@extends($view_path.'.layouts.master')
@section('content')
<div class="row filter_product">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 filter_product_tl">
                <span>{{ str_replace("-"," ",$title) }}</span>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                @php 
                    $ft_pro = 0;
                    $count_ft_pro = count($product);
                @endphp    

                @foreach($product as $prod => $pro)
                    @if($ft_pro == 0)
                        <div class="row">
                    @endif

                    <div class="col-md-3 col-sm-6 col-xs-12 flt_product_con">
                        @foreach($pro->attr_id as $prd_at)
                            <input type="hidden" class="product-attr_pro_{{ $pro->id }}" data-id="{{ $prd_at }}" />
                        @endforeach

                        <input type="hidden" class="product-qty" value="1" />
                        <input type="hidden" class="weight" data-value="{{ $pro->weight }}" />
                    
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ url('/product') }}/{{ $pro->name_alias }}">
                                    <img src="{{ asset('components/front/images/product') }}/{{ $pro->id }}/{{ $pro->image }}" class="img-responsive img_center" />
                                </a>

                                @if($pro->discount_status == 'y' && $pro->discount_type == 'p')
                                <div class="flt_img_disc">
                                    <div class="flt_div_disc">{{ $pro->discount_amount }}%</div>
                                </div>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 flt_product_name">
                                <a href="{{ url('/product') }}/{{ $pro->name_alias }}" class="name_elipse">{{ $pro->name }}</a>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 flt_product_cat">
                                {{ $pro->category }}
                            </div>

                            @if($pro->discount_status == 'y')
                                <div class="col-md-12 col-sm-12 col-xs-12 flt_product_disc">
                                    Rp {{ number_format($pro->price),0,',','.' }}
                                </div>

                                @php
                                    $price = $pro->discount;
                                @endphp
                            @else
                                @php
                                    $price = $pro->price;
                                @endphp

                                <div class="col-md-12 col-sm-12 col-xs-12 flt_product_disc_none">
                                    &nbsp;
                                </div>
                            @endif     
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 flt_product_rl">
                                <div class="row flt_product_block">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="row">
                                            Rp {{ number_format($price),0,',','.' }}
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-2 flt_product_icon_n">
                                        <div class="row">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <div class="col-md-12 col-sm-12 col-xs-12 flt_cart_button">
                                <button class="btn add-cart-front" data-id="{{ $pro->id }}" data-name="pro">TAMBAH KE BAG</button>
                            </div>            
                        </div>
                    </div>

                    @php
                        $ft_pro++;
                    @endphp

                    @if($count_ft_pro > $lmt)
                        @php
                            $prod = $prod + 1;
                        @endphp

                        @if($count_ft_pro - $prod != 0)
                            @if($ft_pro == 4)
                                </div>

                                @php
                                    $ft_pro = 0;
                                @endphp
                            @endif
                        @else
                            </div>
                        @endif
                    @else
                        @if($ft_pro == $count_ft_pro)
                            </div>
                        @endif
                    @endif
                    @endforeach

                <div class="row center">
                    @if(count($product) > 0)
                        <div class="col-md-12 col-sm-12 col-xs-12 flt_pagination">
                            {{ $product->links() }}
                        </div>
                    @else
                        <div class="col-md-12 col-sm-12 col-xs-12 pro_not_found">
                            <h1>{{ $title == "" ? 'Maaf, Halaman yang anda cari tidak ditemukan.' : 'Product Tidak Ditemukan' }}</h1>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection