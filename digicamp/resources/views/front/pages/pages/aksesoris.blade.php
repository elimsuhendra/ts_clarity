@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="cus_container">
			<div class="row akse_con1">
				<div class="col-md-12 col-sm-12 col-xs-12 akse_tl">
					<div class="row">
						<h1>AKSESORIS / ALL PRODUCT</h1>
					</div>
				</div>

				@php
					$cl = 0;
					$ctm = 0;
				@endphp

				@foreach($category as $cat)
					@php
						$limit = 6;
						$calculate = $cl % $limit;
						$ct_color = $color[$calculate];
						$check_cat = count($cat['data']);
						$cl++;
					@endphp

					@if($check_cat > 0)
					<form id="form_{{ $ctm }}" method="POST" action="{{ url('/category') }}/{{ $cat['category_alias'] }}">
                        {{csrf_field()}}

						<div class="col-md-12 col-sm-12 col-xs-12 akse_content1" style="border-top:30px solid {{ $ct_color }};">
							<div class="row flex_table">
								<div class="col-md-12 col-sm-12 col-xs-12 akse_product_con2">
	                            	<input type="hidden" name="ct_color" value="{{ $ct_color }}" />

			                        <div class="row akse_categor_img" style="background-image:url('{{ asset('components/front/images/product_category') }}/{{ $cat['image'] }}');">
			                          <a class="ct_hm_click" data-id="{{ $ctm }}"></a>
			                        </div>

			                        <div class="row akse_categor_tl" style="background-color:{{ $ct_color }};">
			                        	<a class="ct_hm_click" data-id="{{ $ctm }}"><p>{{ $cat['category_name'] }}</p></a>
			                        </div>
			                    </div>
								
								@foreach($cat['data'] as $cat_pro)
								<div class="col-md-12 col-sm-12 col-xs-12 akse_product_con">
			                        <div class="row">
			                            <div class="col-md-12 col-sm-12 col-xs-12">
			                                <a href="{{ url('/product') }}/{{ $cat_pro->name_alias }}"><img src="{{ asset('components/front/images/product') }}/{{ $cat_pro->id }}/{{ $cat_pro->image }}" class="img-responsive img_center" /></a>

			                                @if($cat_pro->discount_status == 'y' && $cat_pro->discount_type == 'p')
			                                <div class="akse_img_disc">
			                                    <div class="akse_div_disc">{{ $cat_pro->discount_amount }}%</div>
			                                </div>
			                                @endif
			                            </div>

			                            <div class="col-md-12 col-sm-12 col-xs-12 akse_product_name">
			                                <a href="{{ url('/product') }}/{{ $cat_pro->name_alias }}"><p class="name_elipse">{!! $cat_pro->name !!}</p></a>
			                            </div>
			                            
			                            @if($cat_pro->discount_status == 'y')
				                            <div class="col-md-12 col-sm-12 col-xs-12 akse_product_disc">
				                                <p>Rp {{ number_format($cat_pro->price),0,',','.' }}</p>
				                            </div>

				                            @php
			                                    $price = $cat_pro->discount;
			                                @endphp
			                            @else
			                                @php
			                                    $price = $cat_pro->price;
			                                @endphp
			                            @endif     

			                            <div class="col-md-12 col-sm-12 col-xs-12 akse_product_rl">
			                                <p>Rp {{ number_format($price),0,',','.' }}</p>
			                            </div>          
			                        </div>
			                    </div>
			                    @endforeach

			                    <div class="col-md-12 col-sm-12 col-xs-12 akse_dll" style="background-color:{{ $ct_color }}">
			                    	<a class="ct_hm_click" data-id="{{ $ctm }}"><p>Produk Lainnya</p></a>
			                    </div>
							</div>
						</div>
			        </form>
					@endif

					@php
						$ctm++;
					@endphp
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection

@push('custom_scripts')
<script>
$(document).ready(function() {
    $(document).on('click', '.ct_hm_click', function(){
        var id = $(this).data('id');
        $('#form_'+id).submit();
    });
});
</script>
@endpush