@extends($view_path.'.layouts.master')
@section('content')
<div class="row cb_banner" style="background-image:url('{{ asset('components/front/images/mockup/informasi.jpg') }}');">
	<div class="cb_abo_op"></div>

	<div class="cus_container">
		<div class="row cb_banner_con">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<h1>INFORMASI</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container">
		<div class="row info_con">
			<div class="col-md-3 col-sm-3 col-xs-12 info_cus_pill">
				<ul class="nav nav-pills nav-stacked">
					@foreach($tabs as $k => $tb)
				    <li class="{{ $loop->first ? 'active' : '' }} info_list"><a data-toggle="pill" href="#tab_{{ $k }}">{{ $tb->post_name }}</a></li>
				    @endforeach
				</ul>
			</div>

			<div class="col-md-9 col-sm-9 col-xs-12 tab-content">
				@foreach($tabs as $k => $tb)
				<div id="tab_{{ $k }}" class="tab-pane fade {{ $loop->first ? 'in active' : '' }} info_content">
			      <h3>{{ $tb->post_name }}</h3><br>
			      {!! $tb->content !!}
			    </div>
			    @endforeach
			</div>
		</div>
	</div>
</div>
@endsection
