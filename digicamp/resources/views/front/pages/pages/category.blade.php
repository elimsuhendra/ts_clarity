@extends($view_path.'.layouts.master')
@section('content')
<div class="row">
    <div class="container">
        <div class="row cate_con">
            <div class="col-md-12 col-sm-12 col-xs-12 cate_banner_con">
                <div class="row cate_banner">
                    <img src="{{ asset('components/front/images/product_category') }}/{{ $category->banner }}" class="img-responsive img_width" />

                    <div class="cat_abso">
                        <div class="col-md-4 col-sm-6 col-xs-12 cat_absoL">
                           <span>{{ $category->description }}</span>

                        </div>

                        <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-5 col-xs-12 cat_absoR">
                            <div class="row">
                                <span>{{ $category->category_name }}</span>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </div>
</div>

<div class="row cat_product">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 cat_product_tl">
                <span>Produk {{ $category->category_name }}</span>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                @php 
                    $cg_pro = 0;
                    $count_cg_pro = count($product);
                @endphp

                @foreach($product as $prd_tbr)
                    @if($cg_pro == 0)
                        <div class="row">
                    @endif

                    <div class="col-md-3 col-sm-6 col-xs-12 cate_product_con">
                        @foreach($prd_tbr->attr_id as $pro => $prd_at)
                            <input type="hidden" class="product-attr_pr_{{ $prd_tbr->id }}" data-id="{{ $prd_at }}" />
                        @endforeach

                        <input type="hidden" class="product-qty" value="1" />
                        <input type="hidden" class="weight" data-value="{{ $prd_tbr->weight }}" />
                    
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="{{ url('/product') }}/{{ $prd_tbr->name_alias }}">
                                    <img src="{{ asset('components/front/images/product') }}/{{ $prd_tbr->id }}/{{ $prd_tbr->image }}" class="img-responsive img_center" />
                                </a>

                                @if($prd_tbr->discount_status == 'y' && $prd_tbr->discount_type == 'p')
                                <div class="cate_img_disc">
                                    <div class="cate_div_disc">{{ $prd_tbr->discount_amount }}%</div>
                                </div>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 cate_product_name">
                                <a href="{{ url('/product') }}/{{ $prd_tbr->name_alias }}" class="name_elipse">{{ $prd_tbr->name }}</a>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 cate_product_cat">
                                {{ $prd_tbr->category }}
                            </div>

                            @if($prd_tbr->discount_status == 'y')
                                <div class="col-md-12 col-sm-12 col-xs-12 cate_product_disc">
                                    Rp {{ number_format($prd_tbr->price),0,',','.' }}
                                </div>

                                @php
                                    $price = $prd_tbr->discount;
                                @endphp
                            @else
                                @php
                                    $price = $prd_tbr->price;
                                @endphp

                                <div class="col-md-12 col-sm-12 col-xs-12 cate_product_disc_none">
                                    &nbsp;
                                </div>
                            @endif     
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 cate_product_rl">
                                <div class="row cate_product_block">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="row">
                                            Rp {{ number_format($price),0,',','.' }}
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-2 cate_product_icon_n">
                                        <div class="row">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <div class="col-md-12 col-sm-12 col-xs-12 cate_cart_button">
                                <button class="btn add-cart-front" data-id="{{ $prd_tbr->id }}" data-name="pr">TAMBAH KE BAG</button>
                            </div>            
                        </div>
                    </div>

                    @php
                        $cg_pro++;
                    @endphp

                    @if($count_cg_pro > $lmt)
                        @php
                            $pro = $pro + 1;
                        @endphp

                        @if($count_cg_pro - $pro != 0)
                            @if($cg_pro == 4)
                                </div>

                                @php
                                    $cg_pro = 0;
                                @endphp
                            @endif
                        @else
                            </div>
                        @endif
                    @else
                        @if($cg_pro == $count_cg_pro)
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>

            <div class="row">
                @if(count($product) > 0)
                    <div class="col-md-12 col-sm-12 col-xs-12 cate_pagination">
                        {{ $product->links() }}
                    </div>
                @else
                    <div class="col-md-12 col-sm-12 col-xs-12 pro_not_found">
                        <h1>Product Tidak Ditemukan</h1>
                    </div>
                @endif
            </div>  
        </div>
    </div>
</div>
@endsection
