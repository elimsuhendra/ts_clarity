@extends($view_path.'.layouts.master')
@section('content')
<div class="row cb_banner" style="background-image:url('{{ asset('components/front/images/mockup/cara-beli.jpg') }}');">
	<div class="cb_abo_op"></div>

	<div class="cus_container">
		<div class="row cb_banner_con">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<h1>CARA BELI</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="cus_container">
		<div class="row cr_bl_cons">
			@foreach($cara as $cr)
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 cr_bl_con1">
						<span>{{ $cr->post_name }}</span>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 cr_bl_con1_2">
						{!! $cr->content !!}
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
@endsection