<a href="{{url($url)}}">
	<button class="btn green {{isset($class) ? $class : ''}}">
		{!!$label!!}
	</button>
</a>