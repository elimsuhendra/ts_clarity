@if(isset($form_class))
<div class = "{{isset($form_class) ? $form_class : ''}}">
@endif
	<div class="form-group form-md-line-input form-md-floating-label" {{$rnd = str_random(3)}}>
		<input type="{{isset($type) ? $type : 'text'}}" id="form_floating_{{$rnd}}" class="form-control {{isset($class) ? $class : ''}}" name="{{isset($name) ? $name : ''}}" value="{{isset($value) ? $value : ''}}" {{isset($attribute) ? $attribute : ''}}>
		<label for="form_floating_{{$rnd}}">{{isset($label) ? $label : ''}}</label>
		{!!isset($note) ? $note : ''!!}
	</div>
@if(isset($form_class))
</div>
@endif