@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
        <div class="col-md-8">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'post_name','label' => 'Post Title','value' => (old('post_name') ? old('post_name') : ''),'attribute' => 'required autofocus'])!!}
         <!--  {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : ''),'attribute' => 'required'])!!} -->
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'content','class' => 'editor','label' => 'Content','value' => (old('content') ? old('content') : '')])!!}
          <h4>Search Engine Optimization</h4><hr/>
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_title','label' => 'Meta Title','value' => (old('meta_title') ? old('meta_title') : '')])!!}
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'meta_keywords','label' => 'Meta Keywords','value' => (old('meta_keywords') ? old('meta_keywords') : '')])!!}
          {!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'meta_description','label' => 'Meta Description','value' => (old('meta_description') ? old('meta_description') : '')])!!}
        </div>
        <div class="col-md-4">
          <div class=" form-md-line-input form-md-floating-label">
            <div class="row">
              {!!view($view_path.'.builder.file',['name' => 'thumbnail','label' => 'Thumbnail','value' => '','type' => 'file','upload_type' => 'single-image','class' => 'col-md-6','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 600 x 400 px','form_class' => 'col-md-6'])!!}
            </div>
            <label for="category_id">Category</label>
            <select class="select2" name="category_id" id="category_id">
              @foreach($category as $c)
                @if(count($c->subcategory) > 0)
                  <optgroup label="{{$c->category_name}}">
                    @foreach($c->subcategory as $sc)
                      <option value="{{$sc->id}}" {{old('category_id') == $sc->id ? 'selected' : ''}}>{{$sc->category_name}}</option>
                    @endforeach
                  </optgroup>
                @endif
              @endforeach
            </select>
          </div>

          <br>
          <!-- <div class="form-group form-md-line-input form-md-floating-label">
            <label for="tag">Tag</label>
            <select class="select2-tag" name="tag[]" multiple="multiple">
              @foreach($tag as $t)
                <option value="{{$t->tag_name}}" {{old('tag') ? in_array($t->tag_name,old('tag')) ? 'selected' : '' : ''}}>{{$t->tag_name}}</option>
              @endforeach
            </select>
          </div> -->
         <!--  {!!view($view_path.'.builder.text',['type' => 'text','name' => 'publish_date','label' => 'Publish Date','value' => (old('publish_date') ? old('publish_date') : ''),'attribute' => 'readonly required','class' => 'datetimepicker'])!!} -->
          {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Publish','value' => (old('status') ? old('status') : '')])!!}
        </div>
      </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
    <script>

    </script>
@endpush
@endsection
