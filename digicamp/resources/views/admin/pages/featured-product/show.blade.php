@extends('layouts.master')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    {!!$breadcrumb!!}
  </section>
  <script>
    $(document).ready(function(){
      $(".draggable").draggable({
          cursor: 'move',
          revert: 'invalid',
          helper: 'clone',
          distance: 20
      });

      $(".droppable").droppable({
          hoverClass: 'ui-state-active',
          tolerance: 'pointer',
          accept: function (event, ui) {
              return true;
          },
          drop: function (event, ui) {
              var obj;
              var id = ui.draggable.data('parentid');
              var total = $('.editable').length;
              if ($(ui.helper).hasClass('draggable')) {
                if(total_edit() == 12){
                  alert('Maximum product for each featured product is 12');
                }else{
                  obj = $(ui.helper).clone();
                  obj.removeClass('draggable').addClass('editable').removeAttr('style').find("i").remove();
                  obj.append('<input type="hidden" name="product[]" value="'+id+'"><a class="text-danger remove pull-right" data-id="'+id+'"><i class="fa fa-trash"></i></a>');
                  $('.draggable[data-parentid="'+id+'"]').hide();
                  $(this).append(obj);
                }
              }
          }
      }).sortable({
          revert: false
      });
      $(document).on('click','.add_product',function(){
        if(total_edit() == 12){
          alert('Maximum product for each featured product is 12');
        }else{
          var id = $(this).data('id');
          var name = $(this).data('name');
          $('.draggable[data-parentid="'+id+'"]').hide();
          var a = '<div class="form-control ui-draggable ui-draggable-handle ui-draggable-dragging editable">';
          a += name+'<input type="hidden" name="product[]" value="'+id+'">';
          a += '<a class="text-danger remove pull-right" data-id="'+id+'"><i class="fa fa-trash"></i></a></div>';                               
          $('.droppable').append(a);
        }
      });
      $(document).on('click','.remove',function(){
        var id = $(this).data('id');
        $('.draggable[data-parentid="'+id+'"]').show();
        $(this).parent().remove();
      });
      function total_edit(){
        var total = $('.editable').length;
        return total;
      }
    });
  </script>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
        @if (Session::has('message'))
          {!! Session::get('message') !!}
        @endif 
          <div class="box-header">
            <h3 class="box-title">{{$title}}</h3>
            {!!view('builder.link',['url' => $path,'label' => '<i class="fa fa-arrow-left"></i> Back'])!!}
          </div>
          <div class="box-header">
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
              </div>
            @endif
            <form role="form" method="post" action="{{URL::to($path)}}/update/{{$feathd->id}}" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              {!!view('builder.text',['name' => 'nama_featured','value'=>old("nama_featured") ? old("nama_featured") : $feathd->nama_featured,'label'=>'Featured Name','attribute' => 'required autofocus'])!!}
              {!!view('builder.text',['name' => 'valid_from','value'=>old("valid_from") ? old("valid_from") : $feathd->valid_from,'label'=>'Valid From','attribute' => 'required readonly','class' => 'datepicker','form_class' => 'col-md-6 pad-left'])!!}
              {!!view('builder.text',['name' => 'valid_to','value'=>old("valid_to") ? old("valid_to") : $feathd->valid_to,'label'=>'Valid To','attribute' => 'required readonly','class' => 'datepicker','form_class' => 'col-md-6 pad-right'])!!}
              {!!view('builder.radio',['name' => 'fgstatus','value'=>old('fgstatus') ? old('fgstatus') : $feathd->fgstatus,'label'=>'Display','attribute' => 'required','data' => ['y' => 'Active','n' => 'Not Active']])!!}
              <div class="form-group col-md-6 pad-left form-drag">
                <label>All Product</label>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">  
                  @foreach($category as $c)
                     <div class="panel panel-default">
                          <a role="button" class="accordion{{$c->id}}" data-toggle="collapse" data-parent="#accordion{{$c->id}}" href="#collapse{{$c->id}}" aria-expanded="true" aria-controls="collapse{{$c->id}}">
                            <div class="panel-heading" role="tab" id="heading{{$c->id}}">
                              <h4 class="panel-title">
                                  {{$c->nama_category}}
                                  <span class="pull-right">
                                    <i class="indicator fa fa-plus"></i>
                                  </span>
                              </h4>
                            </div>
                          </a>
                          <div id="collapse{{$c->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                              @foreach($c->productcategory as $p)
                                <div class="form-control draggable" data-parentid="{{$p->product->id}}" {{in_array($p->product->id,$tempfeatdt) ? 'style=display:none' : ''}}>
                                  <a href="{{url('products/manage-products')}}/edit/{{$p->product->id}}" target="_blank">{{$p->product->nama_product}}</a>
                                  <a class="pull-right add_product" data-id="{{$p->product->id}}" data-name="{{$p->product->nama_product}}"><i class="fa fa-arrow-right"></i></a>
                                </div>
                              @endforeach
                            </div>
                          </div>
                      </div>
                  @endforeach
                </div>
              </div>
              <div class="form-group col-md-6 droppable">
                <label>All Featured Product</label>
                @foreach($featdt as $p)
                  <div class="form-control ui-draggable ui-draggable-handle ui-draggable-dragging editable">
                    <a href="{{url('products/manage-products')}}/edit/{{$p->id}}" target="_blank">{{$p->nama_product}}</a>
                    <input type="hidden" name="product[]" value="{{$p->id}}">
                    <a class="text-danger remove pull-right" data-id="{{$p->id}}"><i class="fa fa-trash"></i></a>
                  </div>
                @endforeach
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
