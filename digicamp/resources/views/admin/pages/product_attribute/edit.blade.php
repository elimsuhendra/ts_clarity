@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$product_attribute_data->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="form-group form-md-line-input">
        <label>Attribute</label>
        <select class="form-control" name="attribute_id" required>
          <option value="">--Select Attribute--</option>
          @foreach($product_attribute as $pa)
            <option value="{{$pa->id}}" data-type="{{$pa->attribute_type}}" {{$pa->id == $product_attribute_data->attribute_id ? 'selected' : ''}}>{{$pa->attribute_name}}</option>
          @endforeach
        <select>
        <div class="form-control-focus"> </div>
      </div>
      <div class="form-group">
        {!!view($view_path.'.builder.text',['name' => 'name','label' => 'Attribute Name','value' => $product_attribute_data->name])!!}
        {!!view($view_path.'.builder.text',['name' => 'value_color','label' => 'Value','class' => 'colorpickers','value' => $product_attribute_data->value,'form_class' => $product_attribute_data->attribute->attribute_type == 'color' ? 'value_color' : 'value_color hidden'])!!}
        {!!view($view_path.'.builder.text',['name' => 'value_select','label' => 'Value','value' => $product_attribute_data->value,'form_class' => $product_attribute_data->attribute->attribute_type == 'select' ? 'value_select' : 'value_select hidden'])!!}
      </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $(document).on('change','select[name="attribute_id"]',function(){
        a   = $(':selected',this).data('type');
        if(a == 'color'){
          $('.value_color').removeClass('hidden');
          $('.value_select').addClass('hidden');
        }else if(a == 'select'){
          $('.value_color').addClass('hidden');
          $('.value_select').removeClass('hidden');
        }
        $('input[name="value_color"]').val('');
        $('input[name="value_select"]').val('');
      })
    });
  </script>
@endpush
@endsection
