<?php namespace digipos\Libraries;

use digipos\models\Customer_cart;
use Cookie;
use Session;

class Cart{
	private static $cek;

	public static function add($request) {
		//$cart 					= Customer_cart::where('identifier',self::identifier())->first();
		$id 								= $request['id'];
		$attr 								= $request['attr'];
		$qty 								= $request['qty'];
		$weight                             = $request['weight'];
		
		$product_data_attribute_master_id 	= $request['product_data_attribute_master_id'];
		if(session('cart.'.$product_data_attribute_master_id)){
			$cur_qty 						= self::get_qty($product_data_attribute_master_id);
			$qty 							 += (int)$cur_qty;
			$data 							= ['product_id' => $id,'attr' => $attr,'qty' => $qty, 'weight' => $weight];
		}else{
			$data 							= ['product_id' => $id,'attr' => $attr,'qty' => $qty, 'weight' => $weight];
		}
  		session(['cart.'.$product_data_attribute_master_id => $data]);
	}

  public static function update($request){
    $id                 = $request['id'];
    $attr               = $request['attr'];
    $qty                = $request['qty'];
    $weight             = $request['weight'];

    $product_data_attribute_master_id   = $request['product_data_attribute_master_id'];
    $data               = ['product_id' => $id,'attr' => $attr,'qty' => $qty, 'weight' => $weight];
    session(['cart.'.$product_data_attribute_master_id => $data]);
  }

	public static function remove($id){
		Session::forget('cart.'.$id);
	}

	public static function remove_coupon($id){
		Session::forget($id);
	}

	public static function get_qty($product_data_attribute_master_id){
		$qty 	= session('cart.'.$product_data_attribute_master_id)['qty'];
		return $qty ?? 0;
	}

	public static function set_product_data($request){
		$cart 			= session('cart.'.$request->product_data_attribute_master_id);
		$cart['data']	= $request->data_product;
		session(['cart.'.$request->product_data_attribute_master_id => $cart]);
	}

	public static function get_checkout_data() {
		$checkout_data 	= session('checkout_data');
  		return $checkout_data;
	}

	public static function set_checkout_data($checkout_data){
		session(['checkout_data' => $checkout_data]);
	}

	public static function get_cart() {
		$cart 	= session('cart');
  		return $cart;
	}

	public static function get_total_item(){
		$cart 			= session('cart');
		$qty 			= 0;
		if($cart){
			foreach($cart as $c){
				$qty 		+= $c['qty']; 
			}
		}
		return $qty;
	}

	// Custom Cart
	public static function get_coupon() {
		$coupon 	= session('coupon');
  		return $coupon;
	}

	public static function set_coupon($rs_coupon){
		session(['coupon' => $rs_coupon]);
	}
	// End Custom Cart

	public static function identifier() {
		$identifier 	  	  = Cookie::get(env('APP_CART_KEY'));
		return $identifier;
	}

	public static function remove_all(){
		Session::forget('cart');
		Session::forget('checkout_data');
	}
}