<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Slideshow;
use digipos\models\Pages;

class SlideshowController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title = trans('general.slide-show');
		$this->root_link = "slide-show";
		$this->primary_field = "slideshow_name";
		$this->model = new Slideshow;
		$this->bulk_action = true;
		$this->bulk_action_data   = [2];
		$this->image_path = 'components/front/images/slideshow/';
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name'		 	=> 'image',
				'label' 		=> trans('general.image'),
				'sorting' 		=> 'y',
				'type' 			=> 'image',
				'file_opt' 		=> ['path' => $this->image_path]
			],[
				'name' => 'slideshow_name',
				'label' => trans('general.slideshow-name'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'slideshow_name',
						'label' => trans('general.slideshow-name'),
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
					],[
						'name' => 'description',
						'label' => trans('general.description'),
						'type' => 'text'
					],[
						'name' => 'url',
						'label' => trans('general.url'),
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required',
						'form_class' => 'col-md-6'
					],[
						'name' => 'button_text',
						'label' => 'Button Title',
						'type' => 'text',
						'form_class' => 'col-md-6'
					],[
						'name' => 'image',
						'label' => trans('general.image'),
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path, 'width' => '1581', 'height' => '620'],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1581 x 620'
					],[
						'name' => 'status',
						'label' => trans('general.status'),
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required'
					]
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'slideshow_name',
						'label' => trans('general.slideshow-name'),
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
					],[
						'name' => 'description',
						'label' => trans('general.description'),
						'type' => 'text'
					],[
						'name' => 'url',
						'label' => trans('general.url'),
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required',
						'form_class' => 'col-md-6'
					],[
						'name' => 'button_text',
						'label' => 'Button Title',
						'type' => 'text',
						'form_class' => 'col-md-6'
					],[
						'name' => 'image',
						'label' => trans('general.image'),
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path, 'width' => '1581', 'height' => '620'],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1581 x 620'
					],[
						'name' => 'status',
						'label' => trans('general.status'),
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required'
					]
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'image',
								'type' => 'image',
								'file_opt' => ['path' => $this->image_path]
							],[
								'name' => 'slideshow_name',
								'type' => 'text'
							]
						];
	}

	public function Sorting(){
		$this->model = $this->model->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}
}
