<?php namespace digipos\Http\Controllers\Admin;


use Illuminate\Http\Request;
use digipos\Libraries\Alert;
use Illuminate\Validation\Rule;

use File;
use digipos\models\Flash_deal;
use digipos\models\Product;
use digipos\models\Product_data_attribute_master;

class FlashdealController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.flash-deal');
		$this->root_link 		= "flash-deal";
		$this->primary_field 	= "id";
		$this->model 			= new Flash_deal;
		$this->bulk_action 		= true;
		$this->bulk_action_data = [1]; 
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'tanggal',
				'label' => 'Tanggal',
				'sorting' => 'y',
				'type' => 'text',
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		$this->data['title'] = 'List '.$this->title;

		$check = $this->model->select('id','tanggal')->where([['tanggal', '<', date("Y-m-d")], ['status', 'y']])->get();

		if($check){
			foreach($check as $ch){
				$update = $this->model->where('id', $ch->id)->update(['status' => 'n']);
			}
		}

		return $this->build('index');
	}

	public function create(){
		$this->data['title']	= trans('general.create').' '.$this->title;
		return $this->render_view('pages.flash_deal.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$product_id = [];

		$date = date_create($request->tanggal);
		$rs_date = date_format($date,"Y-m-d");

		$check = Flash_deal::where('tanggal', $rs_date)->count();

		if($check > 0){
			Alert::fail('Tanggal Sudah ada');
			return redirect()->back();
		}else{
			$this->model->tanggal 		= $rs_date;
			$this->model->valid_from	= $rs_date;
			$this->model->valid_to		= $rs_date;
			$this->model->status 		= $request->status;
			$this->model->product_id 	= json_encode($product_id);
			$this->model->save();
		}
		

		Alert::success('Successfully add Flash Deal');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request, $id){
		$pr_o = [];
		$this->data['title']	= trans('general.view').' '.$this->title;
		$this->data['product']	= Product::where('status', 'y')->pluck('name', 'id')->toArray();
		$this->data['flash'] = $this->model->where('id',$id)->first();

		$flash = json_decode($this->data['flash']->product_id);

		if(count($flash) > 0){
			foreach($flash as $k => $pr_flash){
				$get_product = Product::select('name')->where('id', $pr_flash->product_id)->first();

				if(count($get_product) > 0){
					$pr_o[$k]	= [
								 'product_id'	=> $pr_flash->product_id,
								 'product_name' => $get_product->name,
								 'stock'		=> $pr_flash->stock,
								 'discount'		=> $pr_flash->discount,
							  	];
				}
			}	
		}

		$this->data['product_flash'] = $pr_o;
		return $this->render_view('pages.flash_deal.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$pr_o = [];
		$this->data['title']	= trans('general.edit').' '.$this->title;
		$this->data['product']	= Product::where('status', 'y')->pluck('name', 'id')->toArray();
		$this->data['flash'] = $this->model->where('id',$id)->first();

		$flash = json_decode($this->data['flash']->product_id);
		if(count($flash) > 0){
			foreach($flash as $k => $pr_flash){
				$get_product = Product::select('name')->where('id', $pr_flash->product_id)->first();

				if(count($get_product) > 0){
					$pr_o[$k]	= [
								 'product_id'	=> $pr_flash->product_id,
								 'product_name' => $get_product->name,
								 'stock'		=> $pr_flash->stock,
								 'discount'		=> $pr_flash->discount,
							  	];
				}
			}	
		}

		$this->data['product_flash'] = $pr_o;
		$this->data['product_dam']	= Product_data_attribute_master::get();
		return $this->render_view('pages.flash_deal.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$this->validate($request,[
			'tanggal'		=> 'required',
			'valid_from'	=> 'required',
			'valid_to'		=> 'required',
		]);


		$date = date_create($request->tanggal);
		$valid_from = date_create($request->valid_from);
		$valid_to = date_create($request->valid_to);

		$rs_date = date_format($date,"Y-m-d");
		$rs_valid_from = date_format($valid_from,"Y-m-d H:i:s");
		$rs_valid_to = date_format($valid_to,"Y-m-d H:i:s");

		$rs_check_from = date_format($valid_from,"H:i:s"); 
		$rs_check_to = date_format($valid_to,"H:i:s"); 


		// if($rs_date == $rs_valid_from && $rs_date == $rs_valid_to){
		// 	Alert::fail('Tanggal dan tanggal valid harus sama');
		// 	return redirect()->back();
		// } else{
			if($rs_check_to <= $rs_check_from){
				Alert::fail('Jam Valid to harus jam sesudah valid from');
				return redirect()->back();
			} else{
				$this->model 				= $this->model->find($id);
				$this->model->tanggal 		= $date;
				$this->model->valid_from 	= $rs_check_from;
				$this->model->valid_to 		= $rs_check_to;
				$this->model->status 		= $request->status;
				$this->model->save();

				Alert::success('Successfully update Flash Deal');
				return redirect()->to($this->data['path']);
			}
		// }
		
	}

	public function product_get(Request $request){
		$flash 		= Flash_deal::select('product_id')->where('id', $request->id)->first();
		$rs_card = [];

		$temp = [
				'product_id'   => $request->product_id,
				'stock'		   => $request->stock,
				'discount'		   => $request->price,
	    		];

		if(count($flash->product_id) > 0){
			$con_flash = json_decode($flash->product_id);
			$match = false;

			foreach($con_flash as $j => $cons){
				$product_dam = Product_data_attribute_master::where('product_id', $cons->product_id)->first();

				if($product_dam->stock < $temp['stock']){
					if($temp['product_id'] == $cons->product_id){
						$con_flash[$j]->stock = $temp['stock'];
						$con_flash[$j]->discount = $temp['discount'];

						$match = true;
						break;
					}
				}
			}

			if($match == false){
				$con_flash[] = (object)$temp;
			} 
		} else{
			$con_flash[] = (object)$temp;
		}

		$update = Flash_deal::where('id', $request->id)->update(['product_id' => json_encode($con_flash)]);
		return response()->json(['status' => 'success']);
	}

	public function product_edt(Request $request){
		$flash 		= Flash_deal::select('product_id')->where('id', $request->id)->first();


		if(count($flash->product_id) > 0){
			$con_flash = json_decode($flash->product_id);

			foreach($con_flash as $j => $cons){
				if($request->product_id == $cons->product_id){
					return response()->json(['status' => 'success', 'product_id' => $cons->product_id, 'stock' => $cons->stock, 'discount' => $cons->discount]);
				}
			}
		} 

	}

	public function delete_product($id, $product){
		$flash = Flash_deal::select('product_id')->where('id', $id)->first();

		if($flash->product_id){
			$con_flash = json_decode($flash->product_id);
		} 

		if(count($flash->product_id) > 0){
			foreach($con_flash as $k => $del_flash){
				if($del_flash->product_id == $product){
                	array_forget($con_flash, $k);
				} 
			}
			
            $update = Flash_deal::where('id', $id)->update(['product_id' => json_encode($con_flash)]);

            Alert::success('Remove Product Success.');
			return back();
		} else{
			Alert::fail('Product Not Found.');
			return back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		// $this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'image',
								'type' => 'image',
								'file_opt' => ['path' => $this->image_path]

							],[
								'name' => 'category_name',
								'type' => 'text'
							]
						];
	}

	public function sorting(){
		$this->model = $this->model->where('category_id',0)->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}
}
