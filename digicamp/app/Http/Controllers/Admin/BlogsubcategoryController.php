<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Category;

class BlogsubcategoryController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.sub-category');
		$this->root_link 		= "sub-category";
		$this->primary_field 	= "category_name";
		$this->model 			= new Category;
		$this->check_relation 	= ['productcategory'];
		$this->bulk_action 		= true;
		$this->bulk_action_data = [3]; 
		$this->image_path 		= 'components/front/images/blog_category/';
		// $this->tab_data 		= [
		// 							'general'	=> 'General',
		// 							'seo'		=> 'SEO'
		// 						];
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'image',
				'label' => trans('general.image'),
				'sorting' => 'y',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],[
				'name' => 'category_name',
				'label' => trans('general.sub-category-name'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'category_id',
				'label' => trans('general.parent'),
				'search' => 'select',
				'search_data' => $this->get_category(),
				'sorting' => 'y',
				'belongto' => ['method' => 'category','field' => 'category_name']
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model->where('category_id','!=',0);
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'category_name',
						'label' => 'Sub Category Name',
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
						'alias' => 'category_name_alias',
						'tab' => 'general'
					],[
						'name' => 'category_id',
						'label' => 'Parent Category',
						'type' => 'select',
						'class' => 'select2',
						'data' => $this->get_category(),
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					],[					
					// ],[
					// 	'name' => 'meta_title',
					// 	'label' => 'Meta Title',
					// 	'type' => 'text',
					// 	'tab' => 'seo'
					// ],[
					// 	'name' => 'meta_description',
					// 	'label' => 'Meta Description',
					// 	'type' => 'textarea',
					// 	'tab' => 'seo'
					// ],[
					// 	'name' => 'meta_keywords',
					// 	'label' => 'Meta Keywords',
					// 	'type' => 'textarea',
					// 	'tab' => 'seo'
					// ],[
						'name' => 'image',
						'label' => 'Sub Category Image',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 400 x 400',
						'tab' => 'general'
					],[
						'name' => 'status',
						'label' => 'Status Category',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					]
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'category_name',
						'label' => 'Sub Category Name',
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
						'alias' => 'category_name_alias',
						'tab' => 'general'
					],[
						'name' => 'category_id',
						'label' => 'Parent Category',
						'type' => 'select',
						'class' => 'select2',
						'data' => $this->get_category(),
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					],[
					// ],[
					// 	'name' => 'meta_title',
					// 	'label' => 'Meta Title',
					// 	'type' => 'text',
					// 	'tab' => 'seo'
					// ],[
					// 	'name' => 'meta_description',
					// 	'label' => 'Meta Description',
					// 	'type' => 'textarea',
					// 	'tab' => 'seo'
					// ],[
					// 	'name' => 'meta_keywords',
					// 	'label' => 'Meta Keywords',
					// 	'type' => 'textarea',
					// 	'tab' => 'seo'
					// ],[
						'name' => 'image',
						'label' => 'Sub Category Image',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 400 x 400',
						'tab' => 'general'
					],[
						'name' => 'status',
						'label' => 'Status Category',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required',
						'tab' => 'general'
					]
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'image',
								'type' => 'image',
								'file_opt' => ['path' => $this->image_path]
							],[
								'name' => 'category_name',
								'type' => 'text'
							]
						];
		$this->order_method = "multiple";
		$this->order_filter = [
								'label' => 'Category',
								'name' => 'category_id',
								'data' => $this->get_category(),
								];
	}
	public function sorting(){
		$this->model = $this->model->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		$this->sorting_config();
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		$a = $this->buildbulkedit();
		return $a;
	}

	public function get_category(){
		$query = Category::where('category_id',0)->where('status','y')->orderBy('order','asc')->pluck('category_name','id')->toArray();
		return $query;
	}
}
