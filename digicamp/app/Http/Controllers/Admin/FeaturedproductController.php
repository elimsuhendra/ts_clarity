<?php namespace digipos\Http\Controllers\Admin;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;

use digipos\models\Featured_product_hd;
use digipos\models\Featured_product_dt;
use digipos\models\Product_category;

class FeaturedproductController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title = trans('general.featured-product');
		$this->root_link = "featured-product";
		$this->primary_field = "featured_product_name";
		$this->model = new Featured_product_hd;
		$this->bulk_action = true;
		$this->bulk_action_data   = [1];
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'featured_name',
				'label' => 'Featured Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->data['title'] = 'List '.$this->title;
		return $this->build('index');
	}

	public function create(){
		$this->data['category']	  	= Product_category::where('status','y')->with('product_data_category.product')->orderBy('order','asc')->get();
		$this->data['title'] 		= 'Add New Featured Product';
		return $this->render_view('pages.featured-product.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(request $request){
		$this->validate($request,[
			'featured_name' => 'required',
			'valid_time'	=> 'required',
			'status'		=> 'required'
		]);
		$this->model->featured_name 	= $request->featured_name;
		$valid_from 					= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[0]));
		$valid_to 						= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[1]));
		$this->model->valid_from 		= date_format(date_create($valid_from),'Y-m-d');
		$this->model->valid_to 			= date_format(date_create($valid_to),'Y-m-d');
		$this->model->status 			= $request->status;
		$this->model->save();

		$temp 	= [];
		foreach($request->product as $p){
			$temp[] 	= [
				'featured_product_hd_id' => $this->model->id,
				'product_id'			 => $p
			];
		}
		Featured_product_dt::insert($temp);

		Alert::success('Successfully add new featured product');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['feathd'] = $this->model->where('id',$id)->with('featured_product_dt.product')->first();
		$this->data['featdt'] = $this->data['feathd']->featured_product_dt;
		$temp = [];
		foreach($this->data['featdt'] as $d){
			array_push($temp,$d->product_id);
		}
		$this->data['tempfeatdt'] = $temp;
		$this->data['category']	  = Product_category::where('status','y')->with('product_data_category.product')->orderBy('order','asc')->get();
		$this->data['title'] 	  = 'Edit Featured Product '.$this->data['feathd']->featured_name;
		return $this->render_view('pages.featured-product.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(request $request,$id){
		$this->validate($request,[
			'featured_name' => 'required',
			'valid_time'	=> 'required',
			'status'		=> 'required'
		]);
		$this->model 					= $this->model->find($id);
		$this->model->featured_name 	= $request->featured_name;
		$valid_from 					= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[0]));
		$valid_to 						= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[1]));
		$this->model->valid_from 		= date_format(date_create($valid_from),'Y-m-d');
		$this->model->valid_to 			= date_format(date_create($valid_to),'Y-m-d');
		$this->model->status 			= $request->status;
		$this->model->save();

		//cara lama delete insert baru
			// $this->model->featured_product_dt()->delete();
			// if($request->product){
			// 	$temp 	= [];
			// 	foreach($request->product as $p){
			// 		$temp[] 	= [
			// 			'featured_product_hd_id' => $this->model->id,
			// 			'product_id'			 => $p
			// 		];
			// 	}
			// 	Featured_product_dt::insert($temp);
			// }

		//cara baru update bila ada datanya
		$this->featured_product  = Featured_product_dt::where('featured_product_hd_id', $id)->pluck('product_id', 'product_id')->toArray();
		$check_arr = array_diff($this->featured_product,$request->product);

		if(count($check_arr) > 0){
			foreach($check_arr as $ca){
				Featured_product_dt::where('product_id', $ca)->delete();
			}
		}

		if($request->product){
			$temp 	= [];
			foreach($request->product as $p){
				$cek = 0;
				if(count($this->featured_product) > 0){
					$cek = Featured_product_dt::where([['featured_product_hd_id', $id], ['product_id', $p]])->count();

				}
				
				if($cek == 0){
					$temp[] 	= [
						'featured_product_hd_id' => $this->model->id,
						'product_id'			 => $p
					];

				}
			}
			
			if(count($temp) > 0){
				Featured_product_dt::insert($temp);
			}
		}

		Alert::success('Successfully edit featured product');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(request $request){
		$hd 	= $this->model->find($request->id);
		$hd->featured_product_dt()->delete();
		$hd->delete();
		Alert::success('Successfully delete featured product');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'featured_name',
								'type' => 'text'
							]
						];
	}

	public function sorting(){
		$this->model = $this->model->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}
}
