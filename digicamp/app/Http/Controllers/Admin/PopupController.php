<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Popup;
use Request;

class PopupController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title = trans('general.popup');
		$this->root_link = "popup";
		$this->primary_field = "popup_name";
		$this->model = new Popup;
		$this->bulk_action = true;
		$this->bulk_action_data   = [2];
		$this->image_path 	= 'components/front/images/popup/';
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'image',
				'label' => trans('general.image'),
				'sorting' => 'y',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],[
				'name' => 'popup_name',
				'label' => trans('general.popup-name'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'popup_name',
						'label' => trans('general.popup-name'),
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
					],[
						'name' => 'url',
						'label' => trans('general.url'),
						'type' => 'text',
						'validation' => 'required',
					],[
						'name' => 'valid_from',
						'label' => trans('general.valid_from'),
						'type' => 'text',
						'attribute' => 'readonly',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'required',
						'class'		=> 'datetimepicker',
						'format'	=> ['datetime' => 'd-m-Y H:i:s']
					],[
						'name' => 'valid_to',
						'label' => trans('general.valid_to'),
						'type' => 'text',
						'attribute' => 'readonly',
						'form_class' => 'col-md-6 pad-right',
						'validation' => 'required',
						'class'		=> 'datetimepicker',
						'format'	=> ['datetime' => 'd-m-Y H:i:s']
					],[
						'name' => 'image',
						'label' => trans('general.image'),
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1024 x 600'
					],[
						'name' => 'status',
						'label' => trans('general.status'),
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required'
					]
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'popup_name',
						'label' => trans('general.popup-name'),
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
					],[
						'name' => 'url',
						'label' => trans('general.url'),
						'type' => 'text',
						'validation' => 'required',
					],[
						'name' => 'valid_from',
						'label' => trans('general.valid_from'),
						'type' => 'text',
						'attribute' => 'readonly',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'required',
						'class'		=> 'datetimepicker',
						'format'	=> ['datetime' => 'd-m-Y H:i:s']
					],[
						'name' => 'valid_to',
						'label' => trans('general.valid_to'),
						'type' => 'text',
						'attribute' => 'readonly',
						'form_class' => 'col-md-6 pad-right',
						'validation' => 'required',
						'class'		=> 'datetimepicker',
						'format'	=> ['datetime' => 'd-m-Y H:i:s']
					],[
						'name' => 'image',
						'label' => trans('general.image'),
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 1024 x 600'
					],[
						'name' => 'status',
						'label' => trans('general.status'),
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required'
					]
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		if(Request::has('valid_from')){
			$valid_from 	= explode(' ',Request::input('valid_from'));
			$valid_from 	= date_format(date_create($valid_from[0]),'Y-m-d').' '.$valid_from[1];
		}else{
			$valid_from 	= date('Y-m-d H:i:s');
		}
		Request::merge(['valid_from' => $valid_from]);
		
		if(Request::has('valid_to')){
			$valid_to	= explode(' ',Request::input('valid_to'));
			$valid_to 	= date_format(date_create($valid_to[0]),'Y-m-d').' '.$valid_to[1];
		}else{
			$tvalid_to 	= date('Y-m-d H:i:s');
		}
		Request::merge(['valid_to' => $valid_to]);
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		if(Request::has('valid_from')){
			$valid_from 	= explode(' ',Request::input('valid_from'));
			$valid_from 	= date_format(date_create($valid_from[0]),'Y-m-d').' '.$valid_from[1];
		}else{
			$valid_from 	= date('Y-m-d H:i:s');
		}
		Request::merge(['valid_from' => $valid_from]);
		
		if(Request::has('valid_to')){
			$valid_to	= explode(' ',Request::input('valid_to'));
			$valid_to 	= date_format(date_create($valid_to[0]),'Y-m-d').' '.$valid_to[1];
		}else{
			$tvalid_to 	= date('Y-m-d H:i:s');
		}
		Request::merge(['valid_to' => $valid_to]);

		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(){
		$this->field = $this->field_edit();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}
}
