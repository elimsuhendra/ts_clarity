<?php namespace digipos\Http\Controllers\Admin;


use Illuminate\Http\Request;
use digipos\Libraries\Alert;
use Illuminate\Validation\Rule;

use File;
use DB;
use digipos\models\Product;
use digipos\models\Price_rule;
use digipos\models\Price_rule_product;

class PriceruleController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.price-rule');
		$this->root_link 		= "price-rule";
		$this->primary_field 	= "id";
		$this->model 			= new Price_rule;
		$this->bulk_action 		= true;
		$this->bulk_action_data = [1]; 
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'rule_name',
				'label' => 'Rule Name',
				'sorting' => 'y',
				'search' => 'text',
				'type' => 'text',
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		$check = Price_rule::select('id')->where([['status', 'y'], ['valid_to', '<', date("Y-m-d")]])->get();
		if($check){
			foreach($check as $ch){
				$update = Price_rule::where('id', $ch->id)->update(['status' => 'n']);
			}
		}

		$this->data['title'] = 'List '.$this->title;
		return $this->build('index');
	}

	public function create(){
		$this->data['title']	 = trans('general.create').' '.$this->title;
		$this->data['type_rule'] = [
										'reseller' => 'Reseller',
										'discount' => 'Discount'
									];
		return $this->render_view('pages.price_rule.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$this->model->rule_name = $request->rule_name;
		$this->model->type = $request->type;
		$this->model->status = $request->status;
		$this->model->data_type = "price-rule";
		$this->model->save();

		Alert::success('Successfully add Price Rule');
		return redirect()->to($this->data['path'].'/'.$this->model->id.'/edit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request, $id){
		$this->data['title']	  = trans('general.edit').' '.$this->title;
		$this->data['price_rule'] = $this->model->find($id);
		$this->data['type_rule']  		=   [
												'reseller' => 'Reseller',
												'discount' => 'Discount'
											];
		$this->data['discount_type']	=   [
												'v' => 'Value',
												'p' => 'Percentage' 
										    ];

		$this->data['product']     		= Product::select('id', 'name', DB::raw('(select id_product from bkm_price_rule_product where bkm_price_rule_product.id_product = bkm_product.id and bkm_price_rule_product.id_price_rule = '.$id.') as id_product'), DB::raw('(select id_price_rule from bkm_price_rule_product where bkm_price_rule_product.id_product = bkm_product.id and bkm_price_rule_product.id_price_rule = '.$id.') as id_price_rule'))->where('status', 'y')->get();

		return $this->render_view('pages.price_rule.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['title']	  = trans('general.edit').' '.$this->title;
		$this->data['price_rule'] = $this->model->find($id);
		$this->data['type_rule']  		=   [
												'reseller' => 'Reseller',
												'discount' => 'Discount'
											];
		$this->data['discount_type']	=   [
												'v' => 'Value',
												'p' => 'Percentage' 
										    ];

		$this->data['product']     		= Product::select('id', 'name', DB::raw('(select id_product from bkm_price_rule_product where bkm_price_rule_product.id_product = bkm_product.id and bkm_price_rule_product.id_price_rule = '.$id.') as id_product'), DB::raw('(select id_price_rule from bkm_price_rule_product where bkm_price_rule_product.id_product = bkm_product.id and bkm_price_rule_product.id_price_rule = '.$id.') as id_price_rule'))->where('status', 'y')->get();

		return $this->render_view('pages.price_rule.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){			
		$get_valid_from  	= date_create($request->valid_from);
		$get_valid_to  		= date_create($request->valid_to);
		$conv_valid_from 	= date_format($get_valid_from,"Y/m/d H:i:s");
		$conv_valid_to 		= date_format($get_valid_to,"Y/m/d H:i:s");

		if($conv_valid_from > $conv_valid_to){
			Alert::fail('Valid to should be more than Valid From');
			return redirect()->back()->with('re_tabs', 'condition');
		} else if($conv_valid_to < date('Y/m/d H:i:s')){
			Alert::fail('Valid to should be more than today');
			return redirect()->back()->with('re_tabs', 'condition');
		}

		$this->model 				= $this->model->find($id);
		$this->model->rule_name		= $request->rule_name;
		$this->model->status 		= $request->status;
		$this->model->valid_from 	= $conv_valid_from;
		$this->model->valid_to   	= $conv_valid_to;

		if($request->type == "reseller"){
			$this->model->from_qty = $request->from_qty;
		}

		$this->model->reduction_type = $request->reduction_type;
		$this->model->reduction_amount = $request->reduction_amount;
		$this->model->save();

		if($request->exc_product) {
			$check = Price_rule_product::where('id_price_rule', $id)->pluck('id_product', 'id_product')->toArray();

			if(count($check) > 0){
				$check_arr = array_diff($check,$request->exc_product);
				if(count($check_arr) > 0){
					foreach($check_arr as $rm_arr){
						$delete = Price_rule_product::where([['id_product', $rm_arr],['id_price_rule', $id]])->delete();
					} 
				}

				$check_arr2 = array_diff($request->exc_product, $check);

				if(count($check_arr2) > 0){
					foreach($check_arr2 as $ch2){
						//if find same id_product with this 
						// Price_rule_product::where('id_product', $ch2)->update(['status' => 0]);

						$pros = new Price_rule_product;
						$pros->id_price_rule = $id;
						$pros->id_product 	 = $ch2;
						// $pros->status 	 	 = 1;
						$pros->save();
					}
				}
				
			} else if(count($check) <= 0){
				foreach($request->exc_product as $ex_pro){
					$pros = new Price_rule_product;
					$pros->id_price_rule = $id;
					$pros->id_product 	 = $ex_pro;
					// $pros->status 	 	 = 1;
					$pros->save();
				}
			}
		} else{
			$check_all = Price_rule_product::where([['id_price_rule', $id], ['id_product', 0]])->count();
			if($check_all > 0){
				$check = Price_rule_product::where([['id_price_rule', $id], ['id_product', '!=', 0]])->pluck('id_product', 'id_product')->toArray();

				if(count($check) > 0){
					foreach($check as $rm_arr){
						if($rm_arr != 0){
							$delete = Price_rule_product::where([['id_product', $rm_arr],['id_price_rule', $id]])->delete();
						}
					} 
				}

				// $prp_find 	= Price_rule_product::where('id_product', 0)->get();
				// if(count($prp_find) > 0){
				// 	foreach($prp_find as $prp){
				// 		Price_rule_product::where('id_product', $prp->id_product)->update(['status' => 0]);
				// 	}
				// }
			} else{
				$pros = new Price_rule_product;
				$pros->id_price_rule = $id;
				$pros->id_product 	 = 0;
				// $pros->status 	 	= 1;
				$pros->save();

				$check = Price_rule_product::where([['id_price_rule', $id], ['id_product', '!=', 0]])->pluck('id_product', 'id_product')->toArray();

				if(count($check) > 0){
					foreach($check as $rm_arr){
						if($rm_arr != 0){
							$delete = Price_rule_product::where([['id_product', $rm_arr],['id_price_rule', $id]])->delete();
						}
					} 
				}
			}
		}

		Alert::success('Successfully update Price Rule');
		return redirect()->to($this->data['path']);
	}

	public function destroy(Request $request){
		// $this->field = $this->field_edit();
		$delete = Price_rule_product::where('id_price_rule', $request->id)->delete();
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function sorting_config(){
		$this->field = [
							[
								'name' => 'image',
								'type' => 'image',
								'file_opt' => ['path' => $this->image_path]

							],[
								'name' => 'category_name',
								'type' => 'text'
							]
						];
	}

	public function sorting(){
		$this->model = $this->model->where('category_id',0)->where('status','y')->orderBy($this->order_field,$this->order_field_by);
		$this->sorting_config();
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->build('dosorting');
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}
}
