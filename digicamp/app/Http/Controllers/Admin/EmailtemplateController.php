<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Orderstatus;

class EmailtemplateController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard); 
		$this->title = "Email Template";
		$this->root_link = "email-template";
		$this->primary_field = "order_status_name";
		$this->model = new Orderstatus;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'order_status_name',
				'label' => 'Email Status',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status_email_cust',
				'label' => 'Status Email Customer',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			],[
				'name' => 'status_email_admin',
				'label' => 'Status Email Admin',
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		if(!\Request::has('orderdata')){
			$this->model = $this->model->orderBy('id','asc');
		}
		return $this->build('index');
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'order_status_name',
						'label' => 'Order Status / Notification Status',
						'type' => 'text',
						'attribute' => 'required autofocus readonly',
					],[
						'name' => 'email_admin',
						'label' => 'Email Admin',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required',
					],[
						'name' => 'title',
						'label' => 'Title',
						'type' => 'text',
						'attribute' => 'required',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'required',
					],[
						'name' => 'subject',
						'label' => 'Subject',
						'type' => 'text',
						'attribute' => 'required',
						'form_class' => 'col-md-6 pad-right',
						'validation' => 'required',
					],[
						'name' => 'status_email_cust',
						'label' => 'Activate email to customer?',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'required'
					],[
						'name' => 'status_email_admin',
						'label' => 'Activate email to admin?',
						'type' => 'radio',
						'data' => ['y' => 'Active Admin','n' => 'Not Active Admin'],
						'attribute' => 'required',
						'form_class' => 'col-md-6',
						'validation' => 'required'
					],[
						'name' => 'email_cust_content',
						'label' => 'Customer email content',
						'type' => 'textarea',
						'class' => 'editor',
						'form_class' => 'col-md-6 pad-left',
					],[
						'name' => 'email_admin_content',
						'label' => 'Admin email content',
						'type' => 'textarea',
						'class' => 'editor',
						'form_class' => 'col-md-6 pad-right',
					]
				];
		return $field;
	}

	public function create(){
		$this->field = $this->field_create();
		return $this->build('create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$this->field = $this->field_create();
		return $this->build('store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->model = $this->model->where('id',$id);
		$this->field = $this->field_edit();
		return $this->build('edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id){
		$this->field = $this->field_edit();
		$this->model = $this->model->where('id',$id);
		return $this->build('update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		$this->model = $this->model->where('id',$id);
		return $this->build('delete');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}
}
