<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Post;
use digipos\models\Category;
use digipos\models\Tag;
use digipos\models\Post_tag;
use digipos\models\Post_related;

use Illuminate\Http\Request;
use digipos\Libraries\Alert;
use File;

class PostController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.post');
		$this->root_link 		= "post";
		$this->primary_field 	= "post_name";
		$this->model 			= new Post;
		$this->bulk_action 		= true;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'thumbnail',
				'label' => trans('general.image'),
				'sorting' => 'y',
				'type' => 'image',
				'file_opt' => ['path' => 'components/front/images/post/','custom_path' => 'id']
			],[
				'name' => 'post_name',
				'label' => trans('general.post-name'),
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'category_id',
				'label' => trans('general.category'),
				'search' => 'select',
				'search_data' => $this->get_category(),
				'sorting' => 'y',
				'belongto' => ['method' => 'category','field' => 'category_name']
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		return $this->build('index');
	}


	public function create(){
		$this->data['title']	= trans('general.create').' '.$this->title;
		$this->data['category'] = Category::where('status','y')->orderBy('category_name','asc')->get();
		$this->data['tag']		= Tag::orderBy('tag_name','asc')->get();
		return $this->render_view('pages.post.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(request $request){
		$this->validate($request,[
			'post_name'		=> 'required|unique:post',
			'category_id'	=> 'required'
		]);
		$this->model->post_name 		= $request->post_name;
		$this->model->post_name_alias 	= $this->build_alias($request->post_name);
		$this->model->description 		= $request->description;
		$this->model->content 			= $request->content;
		$this->model->category_id 		= $request->category_id;
		if($request->publish_date){
			$request->publish_date 		= explode(' ',$request->publish_date);
			$this->model->publish_date 	= date_format(date_create($request->publish_date[0]),'Y-m-d').' '.$request->publish_date[1];
		}else{
			$this->model->publish_date 	= date('Y-m-d H:i:s');
		}
		$this->model->meta_title 		= $request->meta_title;
		$this->model->meta_keywords 	= $request->meta_keywords;
		$this->model->meta_description 	= $request->meta_description;
		$this->model->status 			= $request->status;
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;
		$this->model->save();
		
		//Upload Thumbnail
		$file 	= [
					'name' 		=> 'thumbnail',
					'file_opt' 	=> ['path' => 'components/front/images/post/'.$this->model->id.'/'],
					'old_image'	=> $this->model->thumbnail
				];
		if($request->hasFile('thumbnail')){
			$image = $this->build_image($file);
			$this->model->thumbnail = $image;
		}else{
			if($request->has('remove-single-image-thumbnail') == 'y'){
				File::delete($file['file_opt']['path'].$this->model->thumbnail);
				$this->model->thumbnail = '';
			}
		}
		$this->model->save();

		//Upload Tag
		if($request->has('tag')){
			foreach($request->tag as $t){
				$tag 	= Tag::where('tag_name',$t)->first();
				if(!$tag){
					$tag 					= new Tag;
					$tag->tag_name 			= $t;
					$tag->tag_name_alias	= $this->build_alias($t);
					$tag->upd_by			= auth()->guard($this->guard)->user()->fkuseraccessid;
					$tag->save();
				}
				$posttag 			= new Post_tag;
				$posttag->post_id 	= $this->model->id;
				$posttag->tag_id 	= $tag->id;
				$posttag->save();
			}
		}
		Alert::success('Successfully add new post');
		return redirect()->to($this->data['path'].'/'.$this->model->id.'/edit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['post']  	= $this->model->find($id);
		$this->data['post_tag']	= [];
		foreach($this->data['post']->post_tag as $pt){
			$this->data['post_tag'][] 	= $pt->tag->tag_name;
		}
		$this->data['category'] = Category::where('status','y')->orderBy('category_name','asc')->get();
		$this->data['tag']		= Tag::orderBy('tag_name','asc')->get();
		$this->data['all_post'] = Post::where('status','y')->where('id','!=',$this->data['post']->id)->orderBy('id','desc')->get();
		$this->data['related_post']	= $this->data['post']->post_related()->pluck('post_related_id')->toArray();
		$this->data['title']	= trans('general.view').' '.$this->title.' '.$this->data['post']->post_name;
		return $this->render_view('pages.post.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['post']  	= $this->model->find($id);
		$this->data['post_tag']	= [];
		foreach($this->data['post']->post_tag as $pt){
			$this->data['post_tag'][] 	= $pt->tag->tag_name;
		}
		$this->data['category'] = Category::where('status','y')->orderBy('category_name','asc')->get();
		$this->data['tag']		= Tag::orderBy('tag_name','asc')->get();
		$this->data['all_post'] = Post::where('status','y')->where('id','!=',$this->data['post']->id)->orderBy('id','desc')->get();
		$this->data['related_post']	= $this->data['post']->post_related()->pluck('post_related_id')->toArray();
		$this->data['title']		= trans('general.edit').' '.$this->title.' '.$this->data['post']->post_name;
		return $this->render_view('pages.post.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(request $request,$id){
		$this->validate($request,[
			'post_name'		=> 'required|unique:post,id,'.$id,
			'category_id'	=> 'required'
		]);
		$this->model 					= $this->model->find($id);
		$this->model->post_name 		= $request->post_name;
		$this->model->post_name_alias 	= $this->build_alias($request->post_name);
		$this->model->description 		= $request->description;
		$this->model->content 			= $request->content;
		$this->model->category_id 		= $request->category_id;
		if($request->publish_date){
			$request->publish_date 		= explode(' ',$request->publish_date);
			$this->model->publish_date 	= date_format(date_create($request->publish_date[0]),'Y-m-d').' '.$request->publish_date[1];
		}else{
			$this->model->publish_date 	= date('Y-m-d H:i:s');
		}
		$this->model->meta_title 		= $request->meta_title;
		$this->model->meta_keywords 	= $request->meta_keywords;
		$this->model->meta_description 	= $request->meta_description;
		$this->model->status 			= $request->status;
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;

		//Upload Thumbnail
		$file 	= [
					'name' 		=> 'thumbnail',
					'file_opt' 	=> ['path' => 'components/front/images/post/'.$this->model->id.'/'],
					'old_image'	=> $this->model->thumbnail
				];
		if($request->hasFile('thumbnail')){
			$image = $this->build_image($file);
			$this->model->thumbnail = $image;
		}else{
			if($request->input('remove-single-image-thumbnail') == 'y'){
				File::delete($file['file_opt']['path'].$this->model->thumbnail);
				$this->model->thumbnail = '';
			}
		}
		$this->model->save();

		//Upload Tag
		$this->model->post_tag()->delete();
		if($request->has('tag')){
			foreach($request->tag as $t){
				$tag 	= Tag::where('tag_name',$t)->first();
				if(!$tag){
					$tag 					= new Tag;
					$tag->tag_name 			= $t;
					$tag->tag_name_alias	= $this->build_alias($t);
					$tag->upd_by			= auth()->guard($this->guard)->user()->fkuseraccessid;
					$tag->save();
				}
				$posttag 			= new Post_tag;
				$posttag->post_id 	= $this->model->id;
				$posttag->tag_id 	= $tag->id;
				$posttag->save();
			}
		}

		//Insert Related
		$this->model->post_related()->delete();
		if($request->has('related_post')){
			foreach($request->related_post as $rp){
				$related 					= new Post_related;
				$related->post_id 			= $this->model->id;
				$related->post_related_id	= $rp;
				$related->save();
			}
		}

		Alert::success('Successfully edit '.$this->model->post_name);
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(request $request){
		$post 	= $this->model->find($request->id);
		$post->post_related()->delete();
		$post->post_tag()->delete();
		$post->delete();
		Alert::success('Post has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_category(){
		$query = Category::where('category_id','!=',0)->where('status','y')->orderBy('order','asc')->pluck('category_name','id')->toArray();
		return $query;
	}
}
