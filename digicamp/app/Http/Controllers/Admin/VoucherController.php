<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Voucher;
use digipos\models\Customer_group;
use digipos\models\Voucher_customer_group_data;

use Illuminate\Http\Request;
use digipos\Libraries\Alert;
use File;

class VoucherController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= trans('general.voucher');
		$this->root_link 		= "voucher";
		$this->primary_field 	= "voucher_name";
		$this->model 			= new Voucher;
		$this->bulk_action 		= true;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'voucher_name',
				'label' => 'Voucher Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'voucher_code',
				'label' => 'Voucher Code',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'status',
				'label' => trans('general.status'),
				'sorting' => 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		return $this->build('index');
	}


	public function create(){
		$this->data['title']			= trans('general.create').' '.$this->title;
		$this->data['customer_group']	= Customer_group::orderBy('id','desc')->pluck('customer_group_name','id')->toArray();
		return $this->render_view('pages.voucher.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(request $request){
		$this->validate($request,[
			'voucher_code'	=> 'required|unique:voucher',
			'voucher_name'	=> 'required',
			'total'			=> 'numeric|min:1',
		],
		[
			'total.min' => 'Total Voucher at least must be 1',
		]);
		$this->model->voucher_code 		= $request->voucher_code;
		$this->model->voucher_name	 	= $request->voucher_name;
		$this->model->description 		= $request->description;
		$valid_from 					= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[0]));
		$valid_to 						= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[1]));
		$this->model->valid_from 		= date_format(date_create($valid_from),'Y-m-d');
		$this->model->valid_to 			= date_format(date_create($valid_to),'Y-m-d');
		$this->model->discount_type 	= $request->discount_type;
		$this->model->amount 		 	= $request->amount ? $request->amount : 0;
		$this->model->min_checkout 		= $request->min_checkout ? $request->min_checkout : 0;
		$this->model->total 		 	= $request->total ? $request->total : 0;
		$this->model->total_use_per_user = $request->total_use_per_user ? $request->total_use_per_user : 0;
		$this->model->free_shipping 	= $request->free_shipping;
		$this->model->status 			= $request->status;
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;

		$this->model->save();

		if($request->has('voucher_customer_group_data')){
			$customer_group_data 	= $request->voucher_customer_group_data;
			$temp 					= [];
			foreach($customer_group_data as $cgd){
				$temp[]			 	= [
					'voucher_id'		=> $this->model->id,
					'customer_group_id'	=> $cgd,
					'created_at' 		=> date('Y-m-d H:i:s'),
					'updated_at' 		=> date('Y-m-d H:i:s')
				];
			}
			voucher_customer_group_data::insert($temp);
		}
	
		Alert::success('Successfully add new '.$this->title);
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['voucher']  					= $this->model->find($id);
		$this->data['voucher_customer_group_data']	= $this->data['voucher']->voucher_customer_group_data->pluck('customer_group_id')->toArray();
		$this->data['customer_group']				= Customer_group::orderBy('id','desc')->pluck('customer_group_name','id')->toArray();
		$this->data['title']						= trans('general.edit').' '.$this->title.' '.$this->data['voucher']->voucher_name;
		return $this->render_view('pages.voucher.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['voucher']  					= $this->model->find($id);
		$this->data['voucher_customer_group_data']	= $this->data['voucher']->voucher_customer_group_data->pluck('customer_group_id')->toArray();
		$this->data['customer_group']				= Customer_group::orderBy('id','desc')->pluck('customer_group_name','id')->toArray();
		$this->data['title']						= trans('general.edit').' '.$this->title.' '.$this->data['voucher']->voucher_name;
		return $this->render_view('pages.voucher.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(request $request,$id){
		$this->validate($request,[
			'voucher_name'	=> 'required',
			'total' => 'numeric|min:1',

		],
		[
			'total.min' => 'Total Voucher at least must be 1',
		]);
		$this->model 				 	= $this->model->find($id);
		$this->model->voucher_name	 	= $request->voucher_name;
		$this->model->description 		= $request->description;
		$valid_from 					= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[0]));
		$valid_to 						= preg_replace('/\s+/', '', str_replace('/','-',explode('-',$request->valid_time)[1]));
		$this->model->valid_from 		= date_format(date_create($valid_from),'Y-m-d');
		$this->model->valid_to 			= date_format(date_create($valid_to),'Y-m-d');
		$this->model->discount_type 	= $request->discount_type;
		$this->model->amount 		 	= $request->amount ? $request->amount : 0;
		$this->model->min_checkout 		= $request->min_checkout ? $request->min_checkout : 0;
		$this->model->total 		 	= $request->total ? $request->total : 0;
		$this->model->total_use_per_user = $request->total_use_per_user ? $request->total_use_per_user : 0;
		$this->model->free_shipping 	= $request->free_shipping;
		$this->model->status 			= $request->status;
		$this->model->upd_by			= auth()->guard($this->guard)->user()->id;

		$this->model->save();

		$this->model->voucher_customer_group_data()->delete();
		if($request->has('voucher_customer_group_data')){
			$customer_group_data 	= $request->voucher_customer_group_data;
			$temp 					= [];
			foreach($customer_group_data as $cgd){
				$temp[]			 	= [
					'voucher_id'		=> $this->model->id,
					'customer_group_id'	=> $cgd,
					'created_at' 		=> date('Y-m-d H:i:s'),
					'updated_at' 		=> date('Y-m-d H:i:s')
				];
			}
			voucher_customer_group_data::insert($temp);
		}
	
		Alert::success('Successfully edit '.$this->model->voucher_name);
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(request $request){
		$voucher 	= $this->model->find($request->id);
		$voucher->voucher_customer_group_data()->delete();
		$voucher->delete();
		Alert::success($this->title.' has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export();
	}
}
