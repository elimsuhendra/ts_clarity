<?php namespace digipos\Http\Controllers\Front;


use Illuminate\Http\request;

// Custom
use Hash;
use Image;
use PDF;
use Session;
use App\Mail\MailOrder;
use App\Mail\OrderMail;
use Illuminate\Support\Facades\Mail;

use digipos\Libraries\Alert;
use digipos\Libraries\Cart;
use digipos\Libraries\Checkout;
use digipos\models\Config;
use digipos\models\Customer;
use digipos\models\Customer_cart;
use digipos\models\Customer_address;
use digipos\models\Voucher;
use digipos\models\Product;
use digipos\models\Product_attribute_data;
use digipos\models\Product_data_attribute_master;
use digipos\models\Price_rule;
use digipos\models\Orderhd;
use digipos\models\Orderdt;
use digipos\models\Orderaddress;
use digipos\models\Orderbilling;
use digipos\models\Orderconfirm;
use digipos\models\Orderlog;
use digipos\models\Orderlogstock;
use digipos\models\Orderstatus;
use digipos\models\Ordervoucher;
// End Custom

use digipos\models\Province;
use digipos\models\City;
use digipos\models\Bank_account;
use digipos\models\Sub_district;
use digipos\models\Delivery_service;
use digipos\models\Delivery_service_sub;

class CheckoutController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}


	// Custom
	public function index(request $request){
		$check = 0;
		$check2 = 0;
		$status = $this->ch_check_stock();
		$count = count($status);

 
		foreach($status as $q => $ch){
			if($ch['status'] == "available"){
				$check += 1;
			} 
		}

		if($count != $check) {
			Alert::fail('Sorry, Stock Not Enough.');
			return redirect()->back();
		} else if($count == $check) {
        	$get_coupon = Cart::get_coupon();

        	if($get_coupon != NULL) {
        		if(auth()->guard($this->guard)->check()){
					$get_st_coupon = $this->ch_check_voucher($get_coupon['id']);

					if($get_st_coupon['id'] == 0){
						$check2 = 0;
					} else{
						$check2 = 1;
					}
				} else{
					$check2 = 1;
				}
        	}

			if($check2 == 0) {        	
				$this->data['province'] =  json_decode($this->getprovince($request));
				$this->data['address'] = $this->get_address($request);
				$this->data['bank'] = Bank_account::get();
				$this->data['checkout'] = Cart::get_checkout_data();
				return $this->render_view('pages.checkout.checkout');
			} else{
				Alert::fail('Sorry, Voucher Have a Problem.');
				return redirect()->route('c_page');
			}
		}
	}

	// Confirm Payment
	public function c_payment(){
		return $this->render_view('pages.checkout.checkout_confirm');
	}

	public function ch_payment(request $request){
		$this->validate($request,[
			'ch_no_inv'			=> 'required',
			'ch_name'			=> 'required',
			'ch_date'			=> 'required',
			'ch_bank'			=> 'required',
			'ch_amount'			=> 'required|numeric',
			'ch_file'			=> 'required|image',
		],
		[
			'ch_no_inv.required'			=> 'No Invoice is required',
			'ch_no_inv.exists'				=> 'No Invoice Not Found',
			'ch_name.required'				=> 'Name is required',
			'ch_date.required'				=> 'Date is required',
			'ch_bank.required'				=> 'Bank Destination is required',
			'ch_amount.required'			=> 'Amount is required',
			'ch_amount.numeric'				=> 'Amount Must Be Digit',
			'ch_file.required'				=> 'Transaction script is required',
			'ch_file.image'					=> 'File must be Jpg or Png',
		]);

		

		$orderhd = Orderhd::where('order_id', $request->ch_no_inv)->first();

		if(count($orderhd) > 0){
			if($orderhd->order_status_id == 1) {
				$no_inv = $request->ch_no_inv;

				$path = "components/front/images/confirm_payment/".$no_inv."/";

				if (!file_exists($path)) {
				    mkdir($path, 0777, true);
				}

				$filename = str_random(6) . '_' . str_replace(' ','_',$request->file('ch_file')->getClientOriginalName());   
				$image = Image::make($request->file('ch_file')->getRealPath());
				$image = $image->save($path.$filename);			

				$orderhd = Orderhd::where('order_id', $no_inv)->first();

				$orderconfirm = new Orderconfirm;
				$orderconfirm->orderhd_id = $orderhd->id;
				$orderconfirm->destination = $orderhd->bank_info;
				$orderconfirm->name = $request->ch_name;
				$orderconfirm->amount = $request->ch_amount;
				$orderconfirm->payment_date = $request->ch_date;
				$orderconfirm->notes = $request->ch_notes;
				$orderconfirm->image_name = $path.$filename;
				$orderconfirm->save();

				Orderhd::where('order_id', $no_inv)->update(['order_status_id' => 20]);

				$orderlog = new Orderlog;
				$orderlog->orderhd_id = $orderhd->id;
				$orderlog->order_status_id = 20;
				$orderlog->notes = "Confirm Payment";
				$orderlog->upd_owner = '';
				$orderlog->upd_by = 0;
				$orderlog->save();

				$customer = Customer::where('id', $orderhd->customer_id)->first();
				$mail = Orderstatus::where('id', 2)->first(); 
					
					if($mail->status_email_cust == 'y') {
						$subject = str_replace('#id',$no_inv,$mail->subject);
						$title = str_replace('#id',$no_inv,$mail->title);

						$dt_cust['subject'] = $subject;
						$dt_cust['to'] 		= $customer->email;

						$this->data['title']        = $title;
						$this->data['status']		= 'cust';

						$change_name = ucfirst(str_replace('#cust_name',$request->ch_name,$mail->email_cust_content));

						$this->data['cust_content'] = $change_name;

						Mail::send('front.mail.content', $this->data, function ($message) use($dt_cust){
				        	$message->subject($dt_cust['subject'])
				              ->to($dt_cust['to']);
				         });
					}

					if($mail->status_email_admin == 'y') {
						$subject = str_replace('#id',$no_inv,$mail->subject);
						$title = str_replace('#id',$no_inv,$mail->title);

						$dt_admin['subject'] 	= $subject;
						$dt_admin['to'] 		= $mail->email_admin;

						$this->data['title']        = $title;
						$this->data['status']		= 'admin';
						$this->data['adm_content'] = $mail->email_admin_content;

						Mail::send('front.mail.content', $this->data, function ($message) use($dt_admin){
				        	$message->subject($dt_admin['subject'])
				              ->to($dt_admin['to']);
				         });
					}

				Alert::success('Confirmation Order Success');

				return redirect()->back()->withInput();
			} else{
				return redirect()->back()->withErrors('Sorry, Payment already confirmed.');
			}
		} else {
			return redirect()->back()->withErrors('Sorry, Invoice not Found.');
		}
	}

	public function inv_live(request $request){
		$inv = $request->inv;

		$check = Orderhd::where([['order_id', $inv], ['order_status_id', '1']])->with('orderaddress')->first();

		if($check != NULL) {
			$check1  = explode("-", $check->bank_info);
			$bank = Bank_account::where('bank_account_name', $check1[0])->first();
			$result['name'] = $check->orderaddress->first_name;
			$result['bank_name'] = $bank->bank_account_name;
			$result['bank_id'] = $bank->id;
 			$result['status'] = "success";
 			$result['amount'] = $check->total_amount;
		} else{
			$result['status'] = "failed";
		}

		return response()->json(['result' => $result]);
	}
	// End Confirm Payment

	public function get_address($request) {

		if(auth()->guard($this->guard)->check()){
			$address = auth()->guard($this->guard)->user()->primary_address;

			if($address != NULL) {
				$city = $address->city_id;
				$sub_district = $address->sub_district_id;
				$request->request->add(['city' => $city, 'sub_district' => $sub_district]);
				$get_address = json_decode($this->getsubdistrict($request))->rajaongkir->results;

				$result = [
									'province' => $get_address->province,
									'city' => $get_address->city,
									'sub_district' => $get_address->subdistrict_name
								  ];
			} else {
				$result = "";
			}

			return $result;
		}
	}

	public function checkout_order(request $request){
		$check = 0;
		$check2 = 0;
		$status = $this->ch_check_stock();
		$count = count($status);

 
		foreach($status as $q => $ch){
			if($ch['status'] == "available"){
				$check += 1;
			} 
		} 

		if($count != $check) {
			Alert::fail('Sorry, Stock Not Enough.');
			return redirect()->route('c_page');
		} else if($count == $check) {
			if(!auth()->guard($this->guard)->check()){
				$this->validate($request,[
					'chk_name'			=> 'required',
					'chk_province'		=> 'required',
					'chk_city'			=> 'required',
					'chk_address'		=> 'required',
					'chk_post'			=> 'required|numeric',
					'chk_phone'			=> 'required|numeric',
					'chk_email'			=> 'required|email|unique:customer,email',
				],
				[
					'chk_name.required'			=> 'Nama Anda Wajib diisi',
					'chk_province.required'		=> 'Provinsi Anda Wajib diisi',
					'chk_city.required'			=> 'Kota Anda Wajib diisi',
					'chk_address.required'		=> 'Alamat Anda Wajib diisi',
					'chk_post.required'			=> 'Kode Pos Anda Wajib diisi',
					'chk_phone.required'		=> 'Phone Anda Wajib diisi',
					'chk_email.required'		=> 'Email Anda Wajib diisi',
					'chk_post.numeric'			=> 'Kode Pos Harus berupa angka',
					'chk_phone.numeric'			=> 'Phone Harus berupa angka',
					'chk_email.email'			=> 'Email Anda Tidak Valid',
					'chk_email.unique'			=> 'Email Sudah Digunakan',
				]);

				$customer_id = $this->generate_account($request);
				auth()->guard($this->guard)->loginusingId($customer_id);
				return back();
			} else if(auth()->guard($this->guard)->check()) {
				$this->validate($request,[
					'chk_name'			=> 'required',
					'chk_province'		=> 'required',
					'chk_city'			=> 'required',
					'chk_shipping'		=> 'required',
					'chk_address'		=> 'required',
					'chk_post'			=> 'required|numeric',
					'chk_phone'			=> 'required|numeric',
					'chk_email'			=> 'required|email',
					'chk_bank'			=> 'required',
				],
				[
					'chk_name.required'			=> 'Nama Anda Wajib diisi',
					'chk_province.required'		=> 'Provinsi Anda Wajib diisi',
					'chk_city.required'			=> 'Kota Anda Wajib diisi',
					'chk_shipping.required'		=> 'Courier Anda Wajib diisi',
					'chk_address.required'		=> 'Alamat Anda Wajib diisi',
					'chk_post.required'			=> 'Kode Pos Anda Wajib diisi',
					'chk_phone.required'		=> 'Phone Anda Wajib diisi',
					'chk_email.required'		=> 'Email Anda Wajib diisi',
					'chk_bank.required'			=> 'Payment Method Anda Wajib diisi',
					'chk_post.numeric'			=> 'Kode Pos Harus berupa angka',
					'chk_phone.numeric'			=> 'Phone Harus berupa angka',
					'chk_email.email'			=> 'Email Anda Tidak Valid'
				]);

				$get_coupon = Cart::get_coupon();

				if($get_coupon != NULL) {
					if(auth()->guard($this->guard)->check()){
						$get_st_coupon = $this->ch_check_voucher($get_coupon['id']);

						if($get_st_coupon['id'] == 1){
				    		$check2 = 1;  
				    	} else if($get_st_coupon['id'] == 0){
				    		$check2 = 0;
				    	}
				    } else{
				    	$check2 = 1;
				    }
		    	}

		    	if($check2 == 0) {
					$type = $request->chk_type;
					$cst_shipping = $request->chk_shipping;
					$cart = Cart::get_cart();
					$checkout = Cart::get_checkout_data();
					$pr_weight = ceil($checkout['total_weight'] / 1000);

					if($checkout['coupon_qty'] > 0){
						$get_voucher = Voucher::where('id', $checkout['coupon_id'])->first();

						if($get_voucher->free_shipping == "y") {
							$cst_shipping = 0;
						}	
					}
					

					$cst_total_shipping = $cst_shipping * $pr_weight;

					$invoice_id = $this->invoice_id();
					$coupon = $checkout['coupon'] != NULL ? $checkout['coupon'] : 0;


					if($checkout['total'] == 0){
						$get_ttl = $checkout['coupon_amount'] - $checkout['sub_total'];

						if($get_ttl >= $cst_total_shipping){
							$TTL_amount = 0;
						} else{
							$TTL_amount = $cst_total_shipping - $get_ttl;
						}
					} else{
						$TTL_amount = $checkout['total'] + $cst_total_shipping;
					}

					if($TTL_amount < 0) {
						$TTL_amount = 0;
					}

					$bank = Bank_account::where('id', $request->chk_bank)->first();
			 		
					
					$customer_id = auth()->guard($this->guard)->user()->id;

			 		if($type == "user_new_address" || $type == "user_none_address") {
			 			$update_address = $request->chk_STaddress != NULL ? $request->chk_STaddress : '';

			 			if($update_address != NULL && $update_address == "y") {
			 				$update_new = Customer_address::where('customer_id', $customer_id)->update(['first_name' => $request->chk_name, 'last_name' => '', 'address' => $request->chk_address, 'postal_code' => $request->chk_post, 'province_id' => $request->chk_province, 'city_id' => $request->chk_city, 'sub_district_id' => $request->chk_subdistrict, 'phone' => $request->chk_phone, 'status' => 'y']);
			 			} else if($type == "user_none_address") {
			 				$set_new 					= new Customer_address;
			 				$set_new->first_name 		= $request->chk_name;
			 				$set_new->last_name         = '';
			 				$set_new->customer_id   	= $customer_id;
							$set_new->address    		= $request->chk_address;
							$set_new->postal_code   	= $request->chk_post;
							$set_new->province_id   	= $request->chk_province;
							$set_new->city_id       	= $request->chk_city;
							$set_new->sub_district_id 	= $request->chk_subdistrict;
							$set_new->phone          	= $request->chk_phone;
							$set_new->status        	= 'y';
							$set_new->save();
			 			}
			 		}	

					// Order HD
					$orderhd = new Orderhd;
					$orderhd->order_id = $invoice_id;
					$orderhd->customer_id = $customer_id;
					$orderhd->total_weight = $pr_weight;
					$orderhd->shipping_amount = $cst_shipping;
					$orderhd->voucher_amount = $coupon; 
					$orderhd->sub_total_amount = $checkout['sub_total'] ? $checkout['sub_total'] : '';
					$orderhd->total_shipping_amount = $cst_total_shipping;
					
					// $kode_unik = mt_rand(100, 500);
					// $orderhd->kode_unik = $kode_unik;

					$orderhd->total_amount = $TTL_amount;
					$orderhd->order_status_id = 1;
					$orderhd->payment_method_id = 1;
					$orderhd->bank_info = $bank->bank_account_name.' - '.$bank->description;
					$orderhd->order_date = date("Y-m-d h:i:s");
					$orderhd->cc_count = 0;
					$orderhd->save();

					// Order Voucher
					if($checkout['coupon_qty'] > 0){
						$ordervoucher = new Ordervoucher;
						$ordervoucher->orderhd_id = $orderhd->id;
						$ordervoucher->voucher_code = $get_voucher->voucher_code;
						$ordervoucher->type = $checkout['coupon_type'];
						$ordervoucher->amount = $checkout['coupon_amount'];
						$ordervoucher->free_shipping_status = $get_voucher->free_shipping;
						$ordervoucher->save();
					}	


					$request->request->add(['city' => $request->chk_city, 'sub_district' => $request->chk_subdistrict]);
					$conv_address = json_decode($this->getsubdistrict($request));

					// Order Billing
					$orderbil = new Orderbilling;
					$orderbil->orderhd_id = $orderhd->id;
					$orderbil->first_name = $request->chk_name;
					$orderbil->last_name = '';
					$orderbil->phone = $request->chk_phone;
					$orderbil->address = $request->chk_address;
					$orderbil->postal_code = $request->chk_post;
					$orderbil->province = $conv_address->rajaongkir->results->province;
					$orderbil->city = $conv_address->rajaongkir->results->city;
					$orderbil->sub_district = $conv_address->rajaongkir->results->subdistrict_name;
					$orderbil->save();

					//Order Address
					$orderadd = new Orderaddress;
					$orderadd->orderhd_id = $orderhd->id;
					$orderadd->first_name = $request->chk_name;
					$orderadd->last_name = '';
					$orderadd->phone = $request->chk_phone;
					$orderadd->address = $request->chk_address;
					$orderadd->postal_code = $request->chk_post;
					$orderadd->province = $conv_address->rajaongkir->results->province;
					$orderadd->city = $conv_address->rajaongkir->results->city;
					$orderadd->sub_district = $conv_address->rajaongkir->results->subdistrict_name;
					$orderadd->delivery_service_name = $request->chk_courier;
					$orderadd->save();

					// Order DT
					foreach($cart as $key => $ct) {
						$product = Product::where('id', $ct['product_id'])->with('primary_images')->first();

						// $reseller =  Price_rule::join('Price_rule_product', 'price_rule.id', 'Price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date('Y-m-d h:i:s')],['price_rule.valid_to', '>=', date('Y-m-d h:i:s')],['price_rule_product.id_product', $product->id],['price_rule.from_qty', '>=', $ct['qty']]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'reseller'],['price_rule.valid_from', '<=', date('Y-m-d h:i:s')],['price_rule.valid_to', '>=', date('Y-m-d h:i:s')],['price_rule.from_qty', '>=', $ct['qty']]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('updated_at', 'desc')->orderBy('from_qty', 'desc')->first();

                    	$price_rule = Price_rule::join('Price_rule_product', 'price_rule.id', 'Price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $product->id]])->orwhere([['price_rule_product.id_product', 0]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('updated_at', 'desc')->first();

						$c_price = $product->price;

                		$reseller = $this->reseller_func($product->id, $product->price, $ct['qty']);

                		if(count($reseller) > 0){
		                    $c_price = $reseller->amount;
		                } else{
		                    $prc = $product->price;
		                }

	                    if(count($price_rule) > 0){
	                        if($price_rule->reduction_type == "v"){
	                            if($c_price > $price_rule->reduction_amount){
	                                $c_price = $prc - $price_rule->reduction_amount;
	                            } else{
	                                $c_price = 0;
	                            }
	                        } else if($price_rule->reduction_type == "p"){
	                            $get_pr = $price_rule->reduction_amount / 100;
	                            $conv = $c_price * $get_pr;
	                            $c_price = $c_price - $conv;
	                        }
	                    }	                    

						$orderdt = new Orderdt;
						$orderdt->orderhd_id = $orderhd->id;
						$orderdt->Product_data_attribute_master_id = $key;

						foreach($ct['attr'] as $attr) {
							$get_attr = Product_attribute_data::where('id', $attr)->with('attribute')->first();
							$data[$get_attr->attribute->attribute_name] = $get_attr->name;
						}

						$orderdt->attribute_description = json_encode($data);
						$orderdt->product_code = $product->product_code;
						$orderdt->product_name = $product->name;
						$orderdt->thumbnail = 'components/front/images/product/'.$ct['product_id'].'/'.$product->primary_images['images_name'];
						$orderdt->price = $c_price;
						$orderdt->qty = $ct['qty'];
						$orderdt->weight = $product->weight;
						$orderdt->total_weight = $product->weight * $ct['qty'];
						$orderdt->total_price = $c_price * $ct['qty'];
						$orderdt->save();

						$get_stock = Product_data_attribute_master::where('id', $key)->first();

						$minus_stock = $get_stock->stock - $ct['qty'];
						$plus_hold = $get_stock->hold + $ct['qty'];
						$upd_stock = Product_data_attribute_master::where('id', $key)->update(['stock' => $minus_stock, 'hold' => $plus_hold]);

						// Order Log Stock
						$orderlog_stock = new Orderlogstock;
						$orderlog_stock->orderdt_id = $orderdt->id;
						$orderlog_stock->action = 'hold';
						$orderlog_stock->value = $ct['qty'];
						$orderlog_stock->save();
					}

					// Order Log
					$orderlog = new Orderlog;
					$orderlog->orderhd_id = $orderhd->id;
					$orderlog->order_status_id = 1;
					$orderlog->notes = 'Waiting Payment';
					$orderlog->upd_owner = 'admin';
					$orderlog->upd_by = 1;
					$orderlog->save();
					
					// Customer Cart
					$customer_cart = Customer_cart::where('customer_id', $customer_id)->update(['content' => '[]']);
					
					Cart::remove_all();

					$mail = Orderstatus::where('id', 1)->first(); 
					$this->data['orderhd'] = Orderhd::where('id', $orderhd->id)->first();
					$this->data['orderdt'] = Orderdt::where('orderhd_id', $orderhd->id)->get();
					$this->data['orderaddress'] = Orderaddress::where('orderhd_id', $this->data['orderhd']->id)->first();
					$this->data['customer'] = $request->chk_email;

					if($mail->status_email_cust == 'y') {
						$subject = str_replace('#id',$invoice_id,$mail->subject);
						$title = str_replace('#id',$invoice_id,$mail->title);

						$dt_cust['subject'] = $subject;
						$dt_cust['to'] 		= $request->chk_email;

						$this->data['title']        = $title;
						$this->data['status']		= 'cust';
						$this->data['mail']			= $request->chk_email;

						$change_name = ucfirst(str_replace('#cust_name',$request->chk_name,$mail->email_cust_content));

						$this->data['cust_content'] = $change_name;

						Mail::send('front.mail.order', $this->data, function ($message) use($dt_cust){
				        	$message->subject($dt_cust['subject'])
				              ->to($dt_cust['to']);
				         });
					}

					if($mail->status_email_admin == 'y') {
						$subject = str_replace('#id',$invoice_id,$mail->subject);
						$title = str_replace('#id',$invoice_id,$mail->title);

						$dt_admin['subject'] 	= $subject;
						$dt_admin['to'] 		= $mail->email_admin;

						$this->data['title']        = $title;
						$this->data['status']		= 'admin';
						$this->data['adm_content'] = $mail->email_admin_content;

						Mail::send('front.mail.order', $this->data, function ($message) use($dt_admin){
				        	$message->subject($dt_admin['subject'])
				              ->to($dt_admin['to']);
				         });
					}

					Alert::success('Your Order Success');

					return redirect()->route('order-detail', ['inv' => $invoice_id]);
				} else {
					Alert::fail('Sorry, Voucher Have a Problem.');
					return redirect()->route('c_page');
				}
			}
		}
	}

	// Create Password
	public function generate_account($request){
		$set_pass = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$password = substr(str_shuffle($set_pass), 0, 8);

		$customer 				= new Customer;
		$customer->name 		= $request->chk_name != NULL ? $request->chk_name : '';
		$customer->email 		= $request->chk_email;
		$customer->password 	= Hash::make($password);
		$customer->gender 		= '';
		$customer->status 		= 'y';
		$customer->last_login 	= date('Y-m-d H:i:s');
		$customer->save();

		$result = $customer->id;

		$address 					= new Customer_address;
		$address->customer_id   	= $result;
		$address->first_name    	= $request->chk_name;
		$address->last_name         = '';
		$address->address    		= $request->chk_address;
		$address->postal_code   	= $request->chk_post;
		$address->province_id   	= $request->chk_province;
		$address->city_id       	= $request->chk_city;
		$address->sub_district_id 	= $request->chk_subdistrict;
		$address->phone          	= $request->chk_phone;
		$address->status        	= 'y';
		$address->save();

		$mail = Orderstatus::where('id', 30)->first(); 

		if($mail->status_email_cust == 'y') {
			$dt_cust['subject'] = $mail->subject;
			$dt_cust['to'] 		= $request->chk_email;

			$this->data['title']        = $mail->title;
			$this->data['status']		= 'cust';

			$change_name = ucfirst(str_replace('#cust_name',$request->chk_name,$mail->email_cust_content));

			$change_email = str_replace('#cust_email',$request->chk_email,$change_name);

			$change_password = str_replace('#cust_password',$password,$change_email); 

			$this->data['cust_content'] = $change_password;

			Mail::send('front.mail.content', $this->data, function ($message) use($dt_cust){
	        	$message->subject($dt_cust['subject'])
	              ->to($dt_cust['to']);
	         });
		}

		if($mail->status_email_admin == 'y') {
			$dt_admin['subject'] = $mail->subject;
			$dt_admin['to'] 		= $mail->email_admin;

			$this->data['title']        = $mail->title;
			$this->data['status']		= 'admin';
			$this->data['cust_content'] = $email->email_cust_content;

			Mail::send('front.mail.content', $this->data, function ($message) use($dt_admin){
	        	$message->subject($dt_admin['subject'])
	              ->to($dt_admin['to']);
	         });
		}


		return $result;
	}

	// Create Invoice Id
	public function invoice_id(){
		$get_full_date = date('ymd');
		$get_date = date('d');
		$get_last_id = Orderhd::limit(1)->orderBy('id', 'desc')->first(); 
		
		if($get_last_id != NULL) {
			$check_day =  substr($get_last_id->order_id, 4, 2);
			$limit = 5;
			
			if($check_day == $get_date) {
				$convert_id = substr($get_last_id->order_id, 6);
				$inc_id = $convert_id + 1;
				$get_id_length = strlen($inc_id);
				
				if($limit >= $get_id_length) {
					$set_limit = $limit - $get_id_length;
					$set_id = substr($convert_id, 0, $set_limit);
					$result = $get_full_date.$set_id.$inc_id;
				} else {
					$result = $get_full_date.$inc_id;
				}
			} else{
				$result = $get_full_date."00001";
			}
		} else {
			$result = $get_full_date."00001";
		}

		return $result;
	}
	// End Create Invoice Id

	// Order Detail
	public function order_detail(request $request){
		$inv = $request->inv;
		$invoice = Orderhd::where('order_id', $inv)->first();
		$address = Orderaddress::where('orderhd_id', $invoice->id)->first();
		$product = Orderdt::where('orderhd_id', $invoice->id)->get();

		$this->data['inv'] = $invoice;
		$this->data['address'] = $address;
		$this->data['product'] = $product;
		
		return $this->render_view('pages.checkout.checkout_detail_order');
	}

	// End Order Detail

	// PDF Order Detail
	public function order_detail_pdf($inv) {
		$data    = [];
		$invoice = Orderhd::where('order_id', $inv)->first();

		$data['web_logo'] = Config::where('id', 2)->first();
		$data['order'] = Orderhd::where('order_id', $inv)->with('orderdt','orderaddress')->first();
		$data['phone'] = $this->data['phone'];
		
		$view 				= $this->view_path.'.pdf.order_confirm';
		$title				= 'Invoice order #'.$invoice->order_id.'.pdf';
		$pdf 				= PDF::loadView($view, $data);

		return $pdf->setPaper('a4')->download($title);
	}
	// End PDF Order Detail
	// End Custom

	public function generate($request){
		$this->data['cart'] 			= $this->generateCart($request);
		if($this->data['cart'] == 'no_data'){
			return 'cart';
		}
		$this->data['checkout_data'] 	= $this->data['cart']['checkout_data'];
		$this->data['cart'] 			= $this->data['cart']['cart'];
	}

	public function delivery_information(request $request){
		if($this->generate($request) == 'cart'){
			return redirect('cart');
		}
		$this->data['province'] 		= Province::get();
		return $this->render_view('pages.checkout.delivery_information');
	}

	public function update_delivery_information(request $request){
		$choose_delivery 	= $request->choose_delivery_address;
		$choose_billing 	= $request->choose_billing_address;
		if($choose_delivery == 'new'){
			$this->validate($request,[
				'delivery.first_name'		=> 'required',
				'delivery.last_name'		=> 'required',
				'delivery.address'			=> 'required',
				'delivery.postal_code'		=> 'required|numeric',
				'delivery.province_id'		=> 'required',
				'delivery.city_id'			=> 'required',
				'delivery.sub_district_id'	=> 'required',
				'delivery.phone'			=> 'required|numeric',
			]);
			$data['delivery']	= $request->delivery;
		}else{
			$address 			= auth()->guard($this->guard)->user()->primary_address;
			$data['delivery']	= [
				'first_name'	=> $address->first_name,
				'last_name'		=> $address->last_name,
				'address'		=> $address->address,
				'postal_code'	=> $address->postal_code,
				'province_id'	=> $address->province_id,
				'city_id'		=> $address->city_id,
				'sub_district'	=> $address->sub_district_id,
				'phone'			=> $address->phone
			];
		}
		$data['delivery_type']	= $choose_delivery;

		if(!$choose_billing){
			$this->validate($request,[
				'billing.first_name'		=> 'required',
				'billing.last_name'			=> 'required',
				'billing.address'			=> 'required',
				'billing.postal_code'		=> 'required|numeric',
				'billing.province_id'		=> 'required',
				'billing.city_id'			=> 'required',
				'billing.sub_district_id'	=> 'required',
				'billing.phone'				=> 'required|numeric',
			]);
			$data['billing']	= $request->billing;
		}else{
			$address 			= auth()->guard($this->guard)->user()->primary_address;
			$data['billing']	= [
				'first_name'	=> $address->first_name,
				'last_name'		=> $address->last_name,
				'address'		=> $address->address,
				'postal_code'	=> $address->postal_code,
				'province_id'	=> $address->province_id,
				'city_id'		=> $address->city_id,
				'sub_district'	=> $address->sub_district_id,
				'phone'			=> $address->phone
			];
		}
		$data['billing_type']	= $choose_billing ?? 'new';
		$this->update_delivery_billing_data($data);
		return redirect('checkout/delivery-method');
	}

	public function delivery_method(request $request){
		if($this->generate($request) == 'cart'){
			return redirect('cart');
		}
		$this->data['delivery_service']	= Delivery_service::where('status','y')->orderBy('order','asc')->get();
		return $this->render_view('pages.checkout.delivery_method');
	}

	//Function
	public function delivery_address(request $request){
		$id 				= $request->id;
		$action				= $request->action;
		if($id){
			$customer_address 	= Customer_address::where('customer_id',auth()->guard($this->guard)->user()->id);
			$customer_address->update(['status' => 'n']);
			$customer_address->where('id',$id)->update(['status' => 'y']);
			$this->delivery_address_type($action);
		}
		return 'success';
	}

	public function get_subdistrict(request $request){
		$id 			= $request->id;
		$sub_district 	= Sub_district::where('city_id',$id)->get();
		return response()->json(['sub_district' => $sub_district]);
	}

	function get_delivery_service_sub(request $request){
		$delivery_service_sub 	= Delivery_service_sub::where('delivery_service_id',$request->id)->select('id','name','name_alias')->where('status','y')->orderBy('order','asc')->get();
		return response()->json(['delivery_service_sub' => $delivery_service_sub]);
	}

	function update_delivery_service_sub(request $request){
		$delivery_service_sub 	= Delivery_service_sub::where('delivery_service_id',$request->id)->select('id','name','name_alias')->where('status','y')->orderBy('order','asc')->get();
		return response()->json(['delivery_service_sub' => $delivery_service_sub]);
	}
}