<?php namespace digipos\Http\Controllers\Front;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Request;
use View;
use Cache;
use Cookie;

class Controller extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $data         = [];
    public $root_path    = '/';
    public $view_path    = 'front';
    public $guard        = 'web';
    public $auth_guard   = 'auth:web';
    public $guest_guard  = 'guest:web';

    public function __construct() {
        $this->data['guard']        = $this->guard;
        $this->data['root_path']    = $this->root_path;
        $this->data['view_path']    = $this->view_path;
        //Global Number
        $no = 1;
        if (Request::has('page')){
            $no = Request::input('page') * 10 - 9;
        }
        $this->data['no']       = $no;
        $this->data['my_ip']    = $_SERVER['REMOTE_ADDR'];

        
        $this->initData();
		$this->generateCusMeta();
		$this->generateMenu();
        $this->generateFooter();
        $this->generateCategory();
    }

    public function render_view($view = ''){
        $data = $this->data;
        return view($this->view_path.'.'.$view,$data);       
    }
}
