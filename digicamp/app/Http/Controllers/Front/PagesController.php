<?php namespace digipos\Http\Controllers\Front;
use Illuminate\Http\request;

use digipos\Libraries\Alert;
use digipos\Libraries\Email;
use Validator;
use PDF;
use DB;

use App\Mail\MailOrder;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
// use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use digipos\models\Config;
use digipos\models\Slideshow;
use digipos\models\Subscriber;
use digipos\models\Banner;
use digipos\models\Pages;
use digipos\models\Post;
use digipos\models\Category;
use digipos\models\Product;
use digipos\models\Product_data_attribute_master;
use digipos\models\Featured_product_dt;
use digipos\models\Price_rule;
use digipos\models\Product_category;
use digipos\models\Contactmessage;
use digipos\models\Orderstatus;

class PagesController extends ShukakuController {

	public function __construct(){
		parent::__construct();

		$this->image_path 			= 'components/front/images/pages/';
		$this->data['image_path']  	= $this->image_path;
	}
	
	public function sc_live(request $request){
		$search = $request->search;

		$product = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y'], ['product.name', 'like', '%'.$search.'%']])->select('product.name')->distinct()->get();

		if($product != NULL){
			$result = $product; 
		} else{
			$result['status'] = "No Result";
		}

		return response()->json(['result' => $result]);
	}

	public function search(request $request){
		$result = [];

		$check = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y'], ['product.name', 'like', '%'.$request->hd_search.'%']])->count();

		if($check > 0){
			if ($request->session()->has('hd_search')) {
				$search_result = session('hd_search');
			} else{
				$search_result = $request->hd_search;
				$request->session()->put('hd_search', $request->hd_search);
			}

			$limit = 1;
			$get_page = $request->page != NULL ? $request->page : 1;
			$page  = $get_page;
			$per_page = ($get_page * $limit) - $limit;

	        $search = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y'], ['product.name', 'like', '%'.$search_result.'%']])->select('product.*')->offset($per_page)->limit($limit)->orderBy('product.id', 'desc')->distinct()->get();
			$search_ac = "pro_default";

			$count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.name like '%".$search_result."%' GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

			$count = $count[0]->count;

			$result = $this->checkPriceruleProduct($search, $search_ac, $limit);

			if(count($result) > 0){
	        	$paginator = new Paginator($result, $count, $limit, $page, [
		            'path'  => $request->url(),
		        ]);
	        } else{
				$paginator = [];
	        }

		} else{
	        $request->session()->forget('hd_search');
			$paginator = [];
		}

	    $this->data['product'] = $paginator;

		return $this->render_view('pages.pages.search');
	}

	public function subscribe(request $request){
		$mail = $request->subscribe;

		$subscribe = new Subscriber;
		$subscribe->email = $mail;
		$subscribe->save();

		Alert::success('Thank you for Subscribe Us.');
		return back();
	}

	public function category(Request $request){
		$cat = $request->segment(2);
		$cat  = Product_category::select('id','category_name', 'banner', 'description')->where([['status', 'y'], ['category_name_alias', $cat]])->first();	
		$web_name = Config::select('value')->where('name', 'web_name')->first();

		$this->data['met_tl'] = 'Category '.$cat->category_name.' '.$web_name->value;
		$this->data['met_des'] = 'Category '.$cat->category_name.' '.$web_name->value;
		$this->data['met_key'] = 'Category '.$cat->category_name.' '.$web_name->value;
		$this->data['lmt'] = 4;

		$data = [];
		$temp = [];
		$limit = 30;
		$get_page = $request->page != NULL ? $request->page : 1;
		$page  = $get_page;
		$per_page = ($get_page * $limit) - $limit;


		$product = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y'], ['product.product_category_id', $cat->id]])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->offset($per_page)->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
		$product_ac = "pro_default";

		// $product = Product::select('product.*')->join('product_data_attribute_master', 'product.id', 'product_data_attribute_master.product_id')->where([['product.status', 'y'], ['product_data_attribute_master.stock', '>', 0], ['product.product_category_id', $cat->id]])->groupBy('product_data_attribute_master.product_id')->offset($per_page)->limit($limit)->get();

		$count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.product_category_id = ".$cat->id." GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));
        
        $count = $count[0]->count;
		$result = $this->checkPriceruleProduct($product, $product_ac, $limit);

		// if($count > 0){
		// 	foreach($product as $q => $pro){
		// 		$price_rule = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $pro->id]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('updated_at', 'desc')->first();

	 //            if(count($price_rule) > 0){
	 //                $r_type     = $price_rule->reduction_type;
	 //                $r_amount   = $price_rule->reduction_amount;

	 //                if($r_type == "p"){
	 //                    $get_disc = $r_amount / 100;
	 //                    $res_disc = $pro->price * $get_disc;
	                    
	 //                    if($res_disc > $pro->price){
	 //                        $discount = 0;
	 //                        $status_disc  = "y";
	 //                        $discount_type = "p";
	 //                    } else{
	 //                        $discount = $pro->price - $res_disc;
	 //                        $status_disc  = "y";
	 //                        $discount_type = "p";
	 //                    }
	 //                } else if($r_type == "v"){
	 //                    if($r_amount > $pro->price){
	 //                        $discount = 0;
	 //                        $status_disc  = "y";
	 //                        $discount_type = "v";
	 //                    } else{
	 //                        $discount = $pro->price - $r_amount;
	 //                        $status_disc  = "y";
	 //                        $discount_type = "v";
	 //                    }
	 //                }

	 //            } else{
	 //                $discount     = "";
	 //                $status_disc  = "n";
	 //                $r_amount = "";
	 //                $discount_type = "";
	 //            }

	 //            $temp = [
	 //                        'id'                => $pro->id,
	 //                        'name'              => $pro->name,
	 //                        'name_alias'        => $pro->name_alias,
	 //                        'image'             => $pro->image,
	 //                        'price'             => $pro->price,
	 //                        'discount'          => $discount,
	 //                        'discount_amount'   => $r_amount,
	 //                        'discount_status'   => $status_disc,
	 //                        'discount_type'     => $discount_type,
	 //                    ];
	 //            $temp = (object)$temp;
	 //            array_push($data, $temp);
		// 	}
		if(count($result) > 0){
			$paginator = new Paginator($result, $count, $limit, $page, [
	            'path'  => $request->url(),
	        ]);
		} else{
			$paginator = [];
		}

		$this->data['product'] = $paginator;
		$this->data['category'] = $cat;
		return $this->render_view('pages.pages.category');
	}

	public function filter(request $request, $fil){
		$result = [];
		$not_found = 0;
		$ch_zero = 0;
		$limit = 30;
		$get_page = $request->page != NULL ? $request->page : 1;
		$page  = $get_page;
		$per_page = ($get_page * $limit) - $limit;

		$web_name = Config::select('value')->where('name', 'web_name')->first();

		$met_tl = str_replace('-', ' ',ucfirst($fil));
		$this->data['met_tl'] = $met_tl.' '.$web_name->value;
		$this->data['met_des'] = $met_tl.' '.$web_name->value;
		$this->data['met_key'] = $met_tl.' '.$web_name->value;
		$this->data['lmt'] = 4;

		switch ($fil) {
            case "produk-terlaris":
            	$product_terlaris = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product_data_attribute_master.sold', '>', 0], ['product.status', 'y']])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->offset($per_page)->limit($limit)->orderBy('product_data_attribute_master.sold', 'desc')->groupby('product_data_attribute_master.product_id')->get();

            	$count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product_data_attribute_master.sold > 0 GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));
            	$count = $count[0]->count;

				$product_terlaris_ac = "pro_default";

				$result = $this->checkPriceruleProduct($product_terlaris, $product_terlaris_ac, $limit);
            	break;
           	case "produk-terbaru":
           		$product_terbaru = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y']])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->offset($per_page)->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
				$product_terbaru_ac = "pro_default";

				$count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

				$count = $count[0]->count;

				$result = $this->checkPriceruleProduct($product_terbaru, $product_terbaru_ac, $limit);
           		break;
           	case "produk-rekomendasi":
           		$product_rekomendasi = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('featured_product_dt', 'featured_product_dt.product_id', 'product.id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y'], ['featured_product_dt.featured_product_hd_id', 1]])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->offset($per_page)->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
				$product_rekomendasi_ac = "pro_default";

				$count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_featured_product_dt on bkm_featured_product_dt.product_id = bkm_product.id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_featured_product_dt.featured_product_hd_id = 1 GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

				$count = $count[0]->count;

				$result = $this->checkPriceruleProduct($product_rekomendasi, $product_rekomendasi_ac, $limit);
				break;
			case "promo-terbaru":
				$check_rule = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->where('price_rule_product.id_product', 0)->count();

				if($check_rule > 0){
					$check_rl = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule_product.id_product')->orderBy('price_rule.updated_at', 'desc')->first();

					$check_rl_zero = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")], ['price_rule_product.id_product', 0]])->select('price_rule_product.id_product', 'price_rule.updated_at')->orderBy('price_rule.updated_at', 'desc')->first();

					if($check_rl->id_product == 0){
						$product_promo = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y']])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->offset($per_page)->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
						$product_promo_ac = "pro_default";

						$count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

						$count = $count[0]->count;

						$result = $this->checkPriceruleProduct($product_promo, $product_promo_ac, $limit);
						break;
					} else{
						$arr_rule = [];
						$arr_not_rule = [];
						$arr_join = [];
						$check_ar_l = 0;

						$get_id_promo = DB::select(DB::raw("select bkm_price_rule_product.id_product from bkm_price_rule inner join bkm_price_rule_product on bkm_price_rule_product.id_price_rule = bkm_price_rule.id where bkm_price_rule.status = 'y' and bkm_price_rule.type = 'discount' and bkm_price_rule.valid_from <= '".date("Y-m-d h:i:s")."' and bkm_price_rule.valid_to >= '".date("Y-m-d h:i:s")."' and bkm_price_rule.updated_at > '".$check_rl_zero->updated_at."' order by bkm_price_rule.updated_at desc limit ".$limit." offset ".$per_page." "));

						$count_prom = count($get_id_promo);
						$cal = $page * $limit;
						$cal1 = $cal - $count_prom;

						if($count_prom >= $cal){
							foreach($get_id_promo as $gt_id_pr){
								array_push($arr_rule, $gt_id_pr->id_product);
							}

							$arr_rule = implode(',', $arr_rule);

							$product_promo = DB::select(DB::raw("select bkm_product.*, bkm_product_category.category_name, bkm_product_data_attribute_master.id as pdm from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_product_category on bkm_product_category.id = bkm_product.product_category_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id in (".$arr_rule.") group by bkm_product_data_attribute_master.product_id order by field (bkm_product.id, ".$arr_rule.") ")); 
						} else{
							if($cal1 > 0){
								$cal2 = $cal1 - $limit;

								if($cal2 > 0){
									$cus_offset = $cal2 - 1;
									$get_product = DB::select(DB::raw("select distinct bkm_product.id from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id not in ((select bkm_price_rule_product.id_product from bkm_price_rule inner join bkm_price_rule_product on bkm_price_rule_product.id_price_rule = bkm_price_rule.id where bkm_price_rule.status = 'y' and bkm_price_rule.type = 'discount' and bkm_price_rule.valid_from <= '".date("Y-m-d h:i:s")."' and bkm_price_rule.valid_to >= '".date("Y-m-d h:i:s")."' and bkm_price_rule.updated_at > '".$check_rl_zero->updated_at."'  order by bkm_price_rule.updated_at desc)) order by bkm_product.created_at desc limit ".$limit." offset ".$cus_offset." "));

									foreach($get_product as $gt_pro){
										array_push($arr_not_rule, $gt_pro->id); 
									}

									$arr_not_rule = implode(',', $arr_not_rule);

									$product_promo = DB::select(DB::raw("select bkm_product.*, bkm_product_category.category_name, bkm_product_data_attribute_master.id as pdm from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_product_category on bkm_product_category.id = bkm_product.product_category_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id in (".$arr_not_rule.") group by bkm_product_data_attribute_master.product_id order by field (bkm_product.id, ".$arr_not_rule.") ")); 
								} else if($cal2 < 0){
									$cus_limit = $limit + $cal2;

									$get_product = DB::select(DB::raw("select distinct bkm_product.id from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id not in ((select bkm_price_rule_product.id_product from bkm_price_rule inner join bkm_price_rule_product on bkm_price_rule_product.id_price_rule = bkm_price_rule.id where bkm_price_rule.status = 'y' and bkm_price_rule.type = 'discount' and bkm_price_rule.valid_from <= '".date("Y-m-d h:i:s")."' and bkm_price_rule.valid_to >= '".date("Y-m-d h:i:s")."' and bkm_price_rule.updated_at > '".$check_rl_zero->updated_at."'  order by bkm_price_rule.updated_at desc)) order by bkm_product.created_at desc limit ".$cus_limit."  offset 0"));

									foreach($get_id_promo as $gt_id_pr){
										array_push($arr_rule, $gt_id_pr->id_product);
									}

									foreach($get_product as $gt_pro){
										array_push($arr_not_rule, $gt_pro->id); 
									}

									$arr_join = array_merge($arr_rule,$arr_not_rule);
									$arr_join = implode(',', $arr_join);

									$product_promo = DB::select(DB::raw("select bkm_product.*, bkm_product_category.category_name, bkm_product_data_attribute_master.id as pdm from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_product_category on bkm_product_category.id = bkm_product.product_category_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_product.id in (".$arr_join.") group by bkm_product_data_attribute_master.product_id order by field (bkm_product.id, ".$arr_join.") ")); 
								}
							} 
						}

						$product_promo_ac = "pro_default";
						$count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

						$count = $count[0]->count;

						$result = $this->checkPriceruleProduct($product_promo, $product_promo_ac, $limit);
					}
				} else{
					$product_promo = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->join('price_rule_product', 'price_rule_product.id_product', 'product.id')->join('price_rule', 'price_rule_product.id_price_rule', 'price_rule.id')->join('product_category', 'product.product_category_id' ,'product_category.id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y'], ['price_rule.status', 'y'], ['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('product.*', 'product_category.category_name', 'product_data_attribute_master.id as pdm')->offset($per_page)->limit($limit)->orderBy('product.id', 'desc')->groupby('product_data_attribute_master.product_id')->get();
					$product_promo_ac = "pro_default";

					$count = DB::select(DB::raw("select count(*) as count FROM (select count(*) from bkm_product inner join bkm_product_data_attribute_master on bkm_product.id = bkm_product_data_attribute_master.product_id inner join bkm_price_rule_product on bkm_price_rule_product.id_product = bkm_product.id inner join bkm_price_rule on bkm_price_rule_product.id_price_rule = bkm_price_rule.id where bkm_product.status = 'y' and bkm_product_data_attribute_master.stock > 0 and bkm_price_rule.status = 'y' and bkm_price_rule.type = 'discount' and bkm_price_rule.valid_from <= '".date("Y-m-d h:i:s")."' and bkm_price_rule.valid_to >= '".date("Y-m-d h:i:s")."' GROUP BY bkm_product_data_attribute_master.product_id) as count_page"));

					$count = $count[0]->count;

					$result = $this->checkPriceruleProduct($product_promo, $product_promo_ac, $limit);
				}
				break;
            default:
				$err = "Maaf, Halaman yang anda cari tidak ditemukan.";
				$not_found = 1;
        }

        if(count($result) > 0){
        	$paginator = new Paginator($result, $count, $limit, $page, [
	            'path'  => $request->url(),
	        ]);
        } else if($not_found == 1){
        	$fil = "";
        	$paginator = [];
        } else{
			$paginator = [];
        }

        $this->data['title'] = $fil;
        $this->data['product'] = $paginator;
		return $this->render_view('pages.pages.filter');
	}

	public function aksesoris(request $request){
		$category  = Product_category::with('product_data_category.product.product_data_attribute_master')->where('status', 'y')->get();

		$meta = Pages::select('meta_title', 'meta_description', 'meta_keywords')->where([['id', 2], ['status', 'y']])->first();

		$this->data['met_tl'] = $meta->meta_title;
		$this->data['met_des'] = $meta->meta_description;
		$this->data['met_key'] = $meta->meta_keywords;

		foreach($category as $l => $cat){
			$data = [];
	 		$temp2 = [];
			$check = 0;

        	$limit = 10;
        	$gets_ids = 0;

			foreach($cat->product_data_category as $j => $prod_data){
				if($limit > $j){
					foreach($prod_data->product->product_data_attribute_master as $pdam){
						if($pdam->stock > 0){
	                        $check++;

	                        if($prod_data->product->id == $gets_ids){
	                        	break;
	                        } else{
	                        	$gets_ids = $prod_data->product->id;
	                        }
	                    } else{
	                        break;
	                    }

						 if($pdam->stock > 0){
	                        $check++;
	                    } else{
	                        break;
	                    }

	                    if($check > 0){
	                    	$price_rule = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $prod_data->product->id]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule.reduction_type', 'price_rule.reduction_amount')->orderBy('price_rule.updated_at', 'desc')->first();

	                        if(count($price_rule) > 0){
	                            $r_type     = $price_rule->reduction_type;
	                            $r_amount   = $price_rule->reduction_amount;

	                            if($r_type == "p"){
	                                $get_disc = $r_amount / 100;
	                                $res_disc = $prod_data->product->price * $get_disc;
	                                
	                                if($res_disc > $prod_data->product->price){
	                                    $discount = 0;
	                                    $status_disc  = "y";
	                                    $discount_type = "p";
	                                } else{
	                                    $discount = $prod_data->product->price - $res_disc;
	                                    $status_disc  = "y";
	                                    $discount_type = "p";
	                                }
	                            } else if($r_type == "v"){
	                                if($r_amount > $prod_data->product->price){
	                                    $discount = 0;
	                                    $status_disc  = "y";
	                                    $discount_type = "v";
	                                } else{
	                                    $discount = $prod_data->product->price - $r_amount;
	                                    $status_disc  = "y";
	                                    $discount_type = "v";
	                                }
	                            }

	                        } else{
	                            $discount     = "";
	                            $status_disc  = "n";
	                            $r_amount = "";
	                            $discount_type = "";
	                        }

	                        $temp = [
	                                    'id'                => $prod_data->product->id,
	                                    'name'              => $prod_data->product->name,
	                                    'name_alias'        => $prod_data->product->name_alias,
	                                    'image'             => $prod_data->product->image,
	                                    'price'             => $prod_data->product->price,
	                                    'discount'          => $discount,
	                                    'discount_amount'   => $r_amount,
	                                    'discount_status'   => $status_disc,
	                                    'discount_type'     => $discount_type,
	                                ];
	                        $temp = (object)$temp;
	                    }
					}

					array_push($data, $temp);
				} else{
					break;
				}
			}


			$cat_data[$cat->category_name] = [
												'category_name'  => $cat->category_name,
												'category_alias' => $cat->category_name_alias,
												'image'			 => $cat->image,
												'data'			 => $data,
											];
		}

		$this->data['category'] = $cat_data;
		$this->data['color'] = $this->generateColor();
		return $this->render_view('pages.pages.aksesoris');
	}

	public function ab_us(request $request){
		// $get_deskripsi = Post::where('id', 12)->first();
		// $get_brand = Post::where('id', 11)->first();
		// $data = [];
		// $temp = [];
		// $brand = [];

		// $meta = Pages::select('meta_title', 'meta_description', 'meta_keywords')->where([['id', 3], ['status', 'y']])->first();

		// $this->data['met_tl'] = $meta->meta_title;
		// $this->data['met_des'] = $meta->meta_description;
		// $this->data['met_key'] = $meta->meta_keywords;

		// $desc = preg_replace( '/<img[^>]+\>/i', "",$get_brand->content);
		// preg_match_all('/(src)=("[^"]*")/i',$get_brand->content,$imgs); 

		// foreach($imgs as $i => $img){
		// 	$temp = preg_replace( '/(width|height|)="\d*"\s/', "", $img);

		// 	foreach($temp as $k => $dt){
		// 		$data[$k] = trim(substr($dt, strpos($dt, 'components')), '"');
		// 	}
		// }

		// $brand = [
		// 			'title' => $get_brand->post_name,
		// 			'desc'  => $desc,
		// 			'image' => $data,
		// 		 ];

		// $this->data['get_deskripsi'] 	= $get_deskripsi;
		// $this->data['get_brand'] 		= $brand;
		$this->data['banner'] 			= Banner::where('id','2')->first();
		// dd($this->data['banner'] );
		return $this->render_view('pages.pages.kontak_servis');
	}

	public function kirim_pesan(request $request){
		$this->validate($request,[
			'name'							=> 'required',
			'email'							=> 'required',
			'no_telp'						=> 'required',
			'address'						=> 'required',
		],
		[
			'name.required'					=> 'Nama Wajib diisi',
			'email.required'				=> 'Nomor telepon wajib diisi',
			'no_telp.numeric'				=> 'Nomor telepon harus angka',
			'address.required'				=> 'Alamat Wajib diisi',
		]);

		$contact 			= new Contactmessage;
		$contact->name 		= $request->name;
		$contact->phone 	= $request->no_telp;
		$contact->email 	= $request->email;
		$contact->message 	= $request->address;
		$contact->save();

		$email_admin = $this->data['web_email'];
		// dd($email_admin);
		$url      = "front.mail.content";
		$title    = "Apply";
		$subject  = "Apply Customer"; 
		$status   = "admin";
		$to 	  = $email_admin;
		$content  = "   <p>Hi admin,&nbsp;</p>
						<p>ada pelanggan yang melakukan apply dengan website, berikut data pelanggan berikut :</p>
						<p>Nama &nbsp; &nbsp; : ".$request->name."</p>
						<p>Email &nbsp; &nbsp; : ".$request->email."</p>
						<p>No Telp &nbsp;: ".$request->no_telp."</p>
						<p>Alamat &nbsp; : ".$request->address."</p>
					";

		$request->request->add([
								'e_url'     => $url,
								'e_title'   => $title,
								'e_subject' => $subject,
								'e_email'	=> $to,
								'e_status'  => $status,
								'e_content' => $content
								]);

		$this->email($request);

       return redirect()->back()->with('message', 'Success, Thanks For Sending Message');
	}

	public function info(request $request){
		$meta = Pages::select('meta_title', 'meta_description', 'meta_keywords')->where([['id', 4], ['status', 'y']])->first();

		$this->data['met_tl'] = $meta->meta_title;
		$this->data['met_des'] = $meta->meta_description;
		$this->data['met_key'] = $meta->meta_keywords;

		$this->data['tabs'] = Post::where('category_id', 8)->get();
		return $this->render_view('pages.pages.informasi');
	}

	public function cr_bl(request $request){
		$meta = Pages::select('meta_title', 'meta_description', 'meta_keywords')->where([['id', 5], ['status', 'y']])->first();

		$this->data['met_tl'] = $meta->meta_title;
		$this->data['met_des'] = $meta->meta_description;
		$this->data['met_key'] = $meta->meta_keywords;

		$this->data['cara'] = Post::where('category_id',10)->get();
		return $this->render_view('pages.pages.cara_beli');
	}

	public function ct_us(request $request){
		$meta = Pages::where([['id', 6], ['status', 'y']])->first();

		$this->data['met_tl'] = $meta->meta_title;
		$this->data['met_des'] = $meta->meta_description;
		$this->data['met_key'] = $meta->meta_keywords;
		$this->data['data'] 	= $meta;

		return $this->render_view('pages.pages.hubungi_kami');
	}

	public function desain_interior(request $request){
		
		return $this->render_view('pages.pages.desain_interior');
	}
}
