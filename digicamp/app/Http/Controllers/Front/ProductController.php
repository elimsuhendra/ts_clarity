<?php namespace digipos\Http\Controllers\Front;
use Illuminate\Http\request;
use digipos\models\Product;
use digipos\models\Product_category;
use digipos\models\Product_data_images;
use digipos\models\product_data_attribute_master;
use digipos\models\Price_rule;
use digipos\models\Featured_product_dt;
// use Cache;

class ProductController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}

	public function index(request $request){
		$alias = $request->segment(2);
		$status_disc = "n";
		$product = [];
		$cus_qty = 0;
		$limit = 10;
				
		$product = Product::with('product_data_attribute_master.product_data_attribute.product_attribute_data.attribute')->where([['status', 'y'], ['name_alias', 'LIKE', '%'.$alias.'%']])->first();

		$get_rekomendasi = Featured_product_dt::where('featured_product_hd_id', 1)->distinct()->pluck('product_id', 'id')->toArray();
		$product_rekomendasi = Product_data_attribute_master::join('product', 'product.id', 'product_data_attribute_master.product_id')->where([['product_data_attribute_master.stock', '>', 0], ['product.status', 'y']])->whereIn('product.id', $get_rekomendasi)->select('product.*')->limit($limit)->inRandomOrder()->groupby('product_data_attribute_master.product_id')->get();
		$product_rekomendasi_ac = "pro_default";

		$category = Product_category::select('category_name')->where('id', $product->product_category_id)->first();

		$price_rule = Price_rule::join('price_rule_product', 'price_rule.id', 'price_rule_product.id_price_rule')->where([['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")],['price_rule_product.id_product', $product->id]])->orwhere([['price_rule_product.id_product', 0],['price_rule.status', 'y'],['price_rule.type', 'discount'],['price_rule.valid_from', '<=', date("Y-m-d h:i:s")],['price_rule.valid_to', '>=', date("Y-m-d h:i:s")]])->select('price_rule.reduction_type', 'price_rule.reduction_amount', 'price_rule_product.*')->orderBy('updated_at', 'desc')->first();

        if($price_rule){
            $r_type     = $price_rule->reduction_type;
            $r_amount   = $price_rule->reduction_amount;

            if($r_type == "p"){
                $get_disc = $r_amount / 100;
                $res_disc = $product->price * $get_disc;
                
                if($res_disc > $product->price){
                    $discount = 0;
                    $status_disc  = "y";
            		$discount_type = "p";
                } else{
                    $discount = $product->price - $res_disc;
                    $status_disc  = "y";
            		$discount_type = "p";
                }
            } else if($r_type == "v"){
                if($r_amount > $product->price){
                    $discount = 0;
                    $status_disc  = "y";
            		$discount_type = "v";
                } else{
                    $discount = $product->price - $r_amount;
                    $status_disc  = "y";
            		$discount_type = "v";
                }
            }

        } else{
            $discount     = "";
            $status_disc  = "n";
            $r_amount = "";
            $discount_type = "";
        }

        if(count($product->product_data_attribute_master) > 0){
        	if($product->product_data_attribute_master[0]->stock > 0){
        		$stock = $product->product_data_attribute_master[0]->stock;
        	} else{
        		$stock = 0;
        	}

        	$attr_id = $product->product_data_attribute_master[0]->id;
        } else{
        	$stock = 0;
        	$attr_id = $product->product_data_attribute_master[0]->id;
        }


        $result_product = [
                    'id'                => $product->id,
                    'name'              => $product->name,
                    'name_alias'        => $product->name_alias,
                    'image'             => $product->image,
                    'category'          => $category->category_name,
                    'price'             => $product->price,
                    'content'           => $product->content,
                    'weight'			=> $product->weight,
                    'discount'          => $discount,
                    'discount_amount'   => $r_amount,
                    'discount_status'   => $status_disc,
                    'discount_type'     => $discount_type,
                    'stock'				=> $stock,
                    'attr_id'			=> $attr_id,
                ];

       	$this->data['product'] = $result_product;
       	$this->data['product_attr'] = $this->generate_product_attribute($product);
		$this->data['product_rekomendasi'] = $this->checkPriceruleProduct($product_rekomendasi, $product_rekomendasi_ac, $limit);

		$this->data['product_image'] = Product_data_images::where('product_id', $product->id)->orderBy('status_primary', 'desc')->get();
		$this->data['lmt'] = 4;
		return $this->render_view('pages.product.product');
	}

	public function ch_stock_pro(request $request){
		$check = product_data_attribute_master::select('stock')->where('id', $request->id_pro)->first();

		if(count($check) > 0){
			$result['status'] = 'success';

			if($check->stock > 0){
				$result['stock_status'] = 'tersedia';
			} else{
				$result['stock_status'] = 'habis';
			}
		} else{
			$result['status'] = 'failed';
		}

		return response()->json(['result' => $result]);
	}

	public function addCart(request $request){
		$request->request->add(['action' => 'add']);
		$result 				= $this->check_stock($request);
		$status 				= $result['status'];
		$result['type']			= 'danger';
		if($status == 'no_product'){
			$result['text']		= 'Product not found';
		}else if($status == 'available'){
			$this->add_cart($request);

			if(auth()->guard($this->guard)->check()){
				$this->sync_dbase_cart($request);
			}

			$result['text']		= 'Product added to cart';
			$result['type']		= 'success';
		}else if($status == 'no_qty'){
			$result['text']		= 'Stock is not enough.<br>Stock left : '.$result['qty'];
			$result['type']		= 'danger';
		}else{
			$result['text']		= 'This attribute is out of stock';
		}
		return response()->json(['result' => $result]);
	}

	public function updateCart(request $request){
		$request->request->add(['action' => 'update']);
		$attribute_master 		= $this->get_attribute($request);		
		$request->request->add(['attr' => $attribute_master->product_data_attribute->pluck('product_attribute_data_id')->toArray()]);
		$request->merge(['id' => $attribute_master->product_id]);
		$result 				= $this->check_stock($request);
		$status 				= $result['status'];
		$result['type']			= 'danger';
		
		if($status == 'no_product'){
			$result['text']		= 'Product not found';
		}else if($status == 'available'){
			$this->update_cart($request);
			$result['text']		= 'Product qty updated';
			$result['type']		= 'success';
		}else if($status == 'no_qty'){
			$result['text']		= 'Stock is not enough';
			$result['type']		= 'danger';
		}else{
			$result['text']		= 'This attribute is out of stock';
		}
		return response()->json(['result' => $result]);
	}

	// Custom update cart
	public function c_updateCart(request $request){
		$result = $this->c_update_cart($request);

		if(auth()->guard($this->guard)->check()){
			$this->sync_dbase_cart($request);
		}

		return response()->json(['result' => $result]);
	}
	// End Custom update cart

	public function removeCart(request $request){
		$id 	= $request->id;	

        $this->remove_cart($id);	

        if(auth()->guard($this->guard)->check()){
			$this->remove_dbase_cart($id);
		}
        return response()->json(['status' => 'success']);
	}

	public function totalCart(){
		$total 	= $this->get_total_cart();
		return $total;
	}

	public function updateCheckoutnotes(request $request){
		$notes 	= $request->notes;
		$this->update_checkout_notes($notes);
		return 'success';
	}
}
