<?php namespace digipos\Http\Controllers\Front;

use Illuminate\Http\request;
use Validator;
use Hash;

use digipos\Libraries\Alert;
use App\Mail\MailOrder;
use Illuminate\Support\Facades\Mail;

use digipos\models\Customer;
use digipos\models\Referal;
use digipos\models\Orderhd;
use digipos\models\Orderstatus;
use digipos\models\Orderdt;
use digipos\models\Orderaddress;
use digipos\models\Customer_address;
use digipos\models\Password_resets;


class AccountController extends ShukakuController {

	public function __construct(){
		$this->middleware($this->guest_guard)->only(['index','index_reg','index_for','login','register','forgot_password','c_forgot_password', 'ch_forgot_password', 'login2']);
		$this->middleware($this->auth_guard)->except(['index','index_reg','index_for','login','register','forgot_password','c_forgot_password', 'ch_forgot_password', 'login2']);
		parent::__construct();
	}

	public function index(){
		return $this->render_view('pages.account.login');
	}

	public function index_reg(){
		return $this->render_view('pages.account.register');
	}

	public function index_for(){
		return $this->render_view('pages.account.forget_password');
	}

	public function login(request $request){
		// $validator 	= Validator::make($request->all(), [
		//     'email'					=> 'required',
		// 	'password'				=> 'required',
		// ]);

		// if($validator->fails()){
		// 	return redirect()->back()->withErrors($validator,'error')->withInput();
		// }

		$this->validate($request,[
			'email'					=> 'required',
			'password'				=> 'required',
		],
		[
			'email.required'		=> 'Email wajib diisi',
			'password.required'		=> 'Password wajib diisi',
		]);


		$email 		= $request->email;
		$password 	= $request->password;
		$auth 		= auth()->guard($this->guard);
		$data = [
			'email' => $email,
			'password' => $password,
			'status' => 'y'
		];
		$auth = $auth->attempt($data);
		if($auth){
        	$this->sync_dbase_cart($request);
			return redirect()->back()->withInput();
		}else{
			Alert::fail('Email / password not found');
			return redirect()->back()->withInput();
		}
	}

	public function login2(request $request){
		$validator 	= Validator::make($request->all(), [
		    'email'					=> 'required',
			'password'				=> 'required',
		]);

		if($validator->fails()){
			return redirect()->back()->withErrors($validator,'login')->withInput();
		}

		$email 		= $request->email;
		$password 	= $request->password;
		$auth 		= auth()->guard($this->guard);
		$data = [
			'email' => $email,
			'password' => $password,
			'status' => 'y'
		];
		$auth = $auth->attempt($data);
		if($auth){
			return redirect()->back()->withInput();
		}else{
			Alert::fail('Email / password not found');
			return redirect()->back()->withInput();
		}
	}

	public function register(request $request){
		// $validator 	= Validator::make($request->all(), [
		//     'email'					=> 'required|unique:customer',
		// 	'password'				=> 'required|min:6|confirmed',
		// 	'password_confirmation' => 'required|min:6',
		// ]);

		// if($validator->fails()){
		// 	$error = $validator->errors();
		// 	$er_message = $error->first();
		// 	Alert::fail($er_message);
		// 	return redirect()->back();
		// }
		
		$this->validate($request,[
			'email'					=> 'required|unique:customer',
			'name'					=> 'required',
			'password'				=> 'required|min:6|confirmed',
			'password_confirmation'	=> 'required|min:6',
		],
		[
			'email.required'					=> 'Email wajib diisi',
			'email.unique'						=> 'Email sudah terdaftar',
			'name.required'						=> 'Nama wajib diisi',
			'password.required'					=> 'Password wajib diisi',
			'password.min'						=> 'Password minimal 6 digit',
			'password.confirmed'				=> 'Password tidak sama',
			'password_confirmation.required'	=> 'Confirmation Password wajib diisi',
			'password_confirmation.min'			=> 'Password Confirmation minimal 6 digit',
		]);

		$customer 				= new Customer;
		$customer->name 		= $request->name != NULL ? $request->name : ' ';
		$customer->email 		= $request->email;
		$customer->password 	= Hash::make($request->password);
		$customer->gender 		= '';
		$customer->status 		= 'y';
		$customer->last_login 	= date('Y-m-d H:i:s');
		$customer->save();

		/* Email */
		$mail = Orderstatus::where('id', 30)->first(); 

		if($mail->status_email_cust == 'y') {
			$dt_cust['subject'] = $mail->subject;
			$dt_cust['to'] 		= $request->email;

			$this->data['title']        = $mail->title;
			$this->data['status']		= 'cust';

			$change_name = ucfirst(str_replace('#cust_name',$request->name,$mail->email_cust_content));

			$change_email = str_replace('#cust_email',$request->email,$change_name);

			$change_password = str_replace('#cust_password',$request->password,$change_email); 

			$this->data['cust_content'] = $change_password;

			Mail::send('front.mail.content', $this->data, function ($message) use($dt_cust){
	        	$message->subject($dt_cust['subject'])
	              ->to($dt_cust['to']);
	         });
		}

		if($mail->status_email_admin == 'y') {
			$dt_admin['subject'] = $mail->subject;
			$dt_admin['to'] 		= $mail->email_admin;

			$this->data['title']        = $mail->title;
			$this->data['status']		= 'admin';
			$this->data['cust_content'] = $email->email_cust_content;

			Mail::send('front.mail.content', $this->data, function ($message) use($dt_admin){
	        	$message->subject($dt_admin['subject'])
	              ->to($dt_admin['to']);
	         });
		}

		auth()->guard($this->guard)->loginusingId($customer->id);

		Alert::success("Register Success.");
		return redirect()->back()->withInput();
	}

	public function profile(request $request){
		$id = auth()->guard($this->guard)->user()->id;

		$this->data['province'] =  json_decode($this->getprovince($request));

		$profile = Customer_address::where([['customer_id', $id],['status', 'y']])->first();

		if($profile != NULL) {
			$city = $profile->city_id;
			$sub_district = $profile->sub_district_id;
			$request->request->add(['city' => $city, 'sub_district' => $sub_district]);
			$get_address = json_decode($this->getsubdistrict($request))->rajaongkir->results;
			$this->data['result'] = [
									'name'        	=> $profile->first_name,
									'address'		=> $profile->address,
									'postal_code'   => $profile->postal_code,
									'province' 		=> $get_address->province,
									'city' 			=> $get_address->city,
									'sub_district' 	=> $get_address->subdistrict_name,
									'phone'			=> $profile->phone,
									'status'		=> 'success'
									];
		} else{
			$this->data['result'] = [
									'status' => 'failed'
									];
		}
		
		return $this->render_view('pages.account.profile');
	}

	public function add_profile(request $request){
		$this->validate($request,[
			'pf_province'		=> 'required',
			'pf_city'			=> 'required',
			'pf_subdistrict'	=> 'required',
			'pf_postal'			=> 'required',
			'pf_phone'			=> 'required',
			'pf_address'		=> 'required',
		],
		[
			'pf_province.required'		=> 'Province is required',
			'pf_city.required'			=> 'City is required',
			'pf_subdistrict.required'	=> 'Subdistrict is required',
			'pf_postal.required'		=> 'Postal Code is required',
			'pf_phone.required'			=> 'Phone is required',
			'pf_address.required'		=> 'Address is required',
		]);

		$id = auth()->guard($this->guard)->user()->id; 
		$name = auth()->guard($this->guard)->user()->name;

		$profile = new Customer_address;
		$profile->customer_id = $id;
		$profile->first_name = $name;
		$profile->last_name = '';
		$profile->address = $request->pf_address;
		$profile->postal_code = $request->pf_postal;
		$profile->province_id = $request->pf_province;
		$profile->city_id = $request->pf_city;
		$profile->sub_district_id = $request->pf_subdistrict;
		$profile->phone = $request->pf_phone;
		$profile->status = 'y';
		$profile->save();

		Alert::success('Add Data Success');
		return redirect()->back()->withInput();
	}

	public function edit_profile(request $request){
		$id = auth()->guard($this->guard)->user()->id;

		$profile = Customer_address::where([['customer_id', $id],['status', 'y']])->first();

		$province = $profile->province_id;
		$city = $profile->city_id;
		$sub_district = $profile->sub_district_id;

		$this->data['province'] =  json_decode($this->getprovince($request)); /* Get Province */

		$request->request->add(['province' => $province]);
		$this->data['city'] = json_decode($this->getcity($request))->rajaongkir->results; /* Get City */

		$request->request->add(['city' => $city]); /* Get Sub district */
		$this->data['sub_district'] = json_decode($this->getsubdistrict($request))->rajaongkir->results;
		

		$request->request->add(['city' => $city, 'sub_district' => $sub_district]);
		$get_address = json_decode($this->getsubdistrict($request))->rajaongkir->results;

		$this->data['result'] = [
								'name'        		=> $profile->first_name,
								'address'			=> $profile->address,
								'postal_code'   	=> $profile->postal_code,
								'province' 			=> $get_address->province_id,
								'city' 				=> $get_address->city_id,
								'sub_district' 		=> $get_address->subdistrict_id,
								'phone'				=> $profile->phone,
								'status'			=> 'success'
								];

		return $this->render_view('pages.account.edit_profile');
	}

	public function update_profile(request $request){
		$this->validate($request,[
			'pf_name'			=> 'required',
			'pf_province'		=> 'required',
			'pf_city'			=> 'required',
			'pf_subdistrict'	=> 'required',
			'pf_postal'			=> 'required',
			'pf_phone'			=> 'required',
			'pf_address'		=> 'required',
		],
		[
			'pf_name.required'			=> 'Name is required',
			'pf_province.required'		=> 'Province is required',
			'pf_city.required'			=> 'City is required',
			'pf_subdistrict.required'	=> 'Subdistrict is required',
			'pf_postal.required'		=> 'Postal Code is required',
			'pf_phone.required'			=> 'Phone is required',
			'pf_address.required'		=> 'Address is required',
		]);

		$id = auth()->guard($this->guard)->user()->id;

		$name = $request->pf_name;
		$province = $request->pf_province;
		$city = $request->pf_city;
		$subdistrict = $request->pf_subdistrict;
		$postal = $request->pf_postal;
		$phone = $request->pf_phone;
		$address = $request->pf_address;

		$update = Customer_address::where('customer_id', $id)->update(['first_name' => $name, 'address' => $address, 'postal_code' => $postal, 'province_id' => $province, 'city_id' => $city, 'sub_district_id' => $subdistrict, 'phone' => $phone]);

		Alert::success('Update Profile Success');
		return redirect()->back()->withInput();
	}

	public function change_password(){
		return $this->render_view('pages.account.change_password');
	}

	public function update_password(request $request){
		$this->validate($request,[
			'old_password'				=> 'required',
			'password'					=> 'required|min:6|confirmed',
			'password_confirmation'		=> 'required|min:6',
		],
		[
			'old_password.required'		=> 'Old Password is required',
			'password.required'			=> 'New Password is required',
			'password.min'				=> 'Password must be 6 digit or more',
			'password.confirmed'		=> 'Password & Confirm Not Match',
			'Cpassword.required'		=> 'Confirm Password is required',
			'Cpassword.min'				=> 'Confirm Password must be 6 digit or more',
		]);

		$id = auth()->guard($this->guard)->user()->id;
		$check = Customer::where('id', $id)->first();

		if (Hash::check($request->old_password, $check->password)) {
			$new_password = Hash::make($request->password);
			$customer = Customer::where('id', $id)->update(['password' => $new_password]);
			Alert::success('Change Password Success');
			return redirect()->back()->withInput();
		} else{
			return redirect()->back()->withErrors(['password Not Found']);
		}
	}

	public function forgot_password(request $request){
		// $validator 	= Validator::make($request->all(), [
		//     'email'					=> 'required|exists:customer,email',
		// ]);

		$this->validate($request,[
			'email'				=> 'required|exists:customer,email',
		],
		[
			'email.required'	=> 'Email wajib diisi',
			'email.exists'		=> 'Email sudah terdaftar',
		]);


		// if($validator->fails()){
		// 	$error = $validator->errors();
		// 	$er_message = $error->first();
		// 	Alert::fail($er_message);
		// 	return redirect()->back();
		// }

		$set_token = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$token = substr(str_shuffle($set_token), 0, 15);

		$password = new Password_resets;
		$password->email = $request->email;
		$password->token = $token;
		$password->save();

		$name = Customer::where('email', $request->email)->first();
		$mail = Orderstatus::where('id', 32)->first(); 

		if($mail->status_email_cust == 'y') {
			$dt_cust['subject'] = $mail->subject;
			$dt_cust['to'] 		= $request->email;

			$this->data['title']        = $mail->title;
			$this->data['status']		= 'cust';

			$change_name = ucfirst(str_replace('#cust_name',$name->name,$mail->email_cust_content));

			$base_url = url('/');
			$link = "<a href=".$base_url.'/forget-password/'.$token.">Link Forgot Password</a>";

			$change_link = str_replace('#link',$link,$change_name);

			$this->data['cust_content'] = $change_link;

			Mail::send('front.mail.content', $this->data, function ($message) use($dt_cust){
	        	$message->subject($dt_cust['subject'])
	              ->to($dt_cust['to']);
	         });
		}

		if($mail->status_email_admin == 'y') {
			$dt_admin['subject'] = $mail->subject;
			$dt_admin['to'] 		= $mail->email_admin;

			$this->data['title']        = $mail->title;
			$this->data['status']		= 'admin';
			$this->data['cust_content'] = $email->email_cust_content;

			Mail::send('front.mail.content', $this->data, function ($message) use($dt_admin){
	        	$message->subject($dt_admin['subject'])
	              ->to($dt_admin['to']);
	         });
		}
		
		Alert::success('Request Success, Please Check Your Email');
		return redirect()->back()->withInput();
	}

	public function c_forgot_password($request){
		$token = $request;

		$check = Password_resets::where([['token', $token], ['count', 0]])->first();
		
		if($check != NULL){
			$this->data['status'] = "found";
			$this->data['token'] = $token;
		} else{
			$this->data['status'] = "failed";
		}

		return $this->render_view('pages.account.forgot_password');
	}

	public function ch_forgot_password(request $request){
		$this->validate($request,[
			'password'					=> 'required|min:6|confirmed',
			'password_confirmation'		=> 'required|min:6'
		],
		[
			'password.required'		=> 'New Password is required',
			'password.min'			=> 'Password must be 6 digit or more',
			'password.confirmed'	=> 'Password & Confirm Not Match',
			'Cpassword.required'	=> 'Confirm Password is required',
			'Cpassword.min'			=> 'Confirm Password must be 6 digit or more',
		]);

		$token = $request->sc_tk;

		$password = Hash::make($request->password);

		$get_cust = Password_resets::where('token', $token)->first();  
		$update = Customer::where('email', $get_cust->email)->update(['password' => $password]);
		$upd = Password_resets::where('token', $token)->update(['count' => 1]);
		
		Alert::success('Your Password change success');
		return redirect()->route('homes');
	}

	public function order_histori(){
		$id = auth()->guard($this->guard)->user()->id;
		$this->data['order'] = Orderhd::where('customer_id', $id)->with('orderstatus')->orderBy('id', 'desc')->paginate(15);
		return $this->render_view('pages.account.order_histori'); 
	}

	// public function order_check(request $request){
	// 	$id = $request->ch_status;
		
	// 	$check = Orderhd::where('id', $id)->with('orderaddress')->first();

	// 	$get_delivery = explode("-", $check->orderaddress->delivery_service_name);
	// 	$delivery = str_replace(' ', '', strtolower($get_delivery[0]));

	// 	$request->request->add(['waybill' => 'SOCAG00183235715', 'courier' => $delivery]);
	// 	$this->data['get_waybill'] = json_decode($this->getwaybill($request))->rajaongkir->result;
	// 	return $this->render_view('pages.account.check_delivery'); 
 // 	}

 	public function order_view(request $request){
 		$inv = $request->inv;
 		$invoice = Orderhd::where('order_id', $inv)->first();
		$address = Orderaddress::where('orderhd_id', $invoice->id)->first();
		$product = Orderdt::where('orderhd_id', $invoice->id)->get();

		$this->data['inv'] = $invoice;
		$this->data['address'] = $address;
		$this->data['product'] = $product;
		
		return $this->render_view('pages.account.view_histori');
 	} 

	public function logout(){
		auth()->guard($this->guard)->logout();
		Alert::success('You have been logout');
		return redirect()->back()->withInput();
	}

	public function primary_address(request $request){
		$customer_address 	= Customer_address::where('customer_id',auth()->guard($this->guard)->user()->id);
		$customer_address->update(['status' => 'n']);
		$customer_address->where('id',$request->id)->update(['status' => 'y']);
		return 'success';
	}
}
