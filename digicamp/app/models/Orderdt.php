<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Orderdt extends Model{
	protected $table 			= 'orderdt';
	protected $orderlogstock 	= 'digipos\models\Orderlogstock';

	public function orderlogstock(){
		return $this->hasMany($this->orderlogstock,'orderdt_id');
	}
}
