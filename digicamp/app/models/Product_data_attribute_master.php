<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product_data_attribute_master extends Model{
	protected $table 	= 'product_data_attribute_master';
	protected $product_data_attribute = 'digipos\models\Product_data_attribute';

	public function product_data_attribute(){
		return $this->hasMany($this->product_data_attribute,'product_data_attribute_master_id');
	}
}
