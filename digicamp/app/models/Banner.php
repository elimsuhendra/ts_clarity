<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table 	= 'banner';
}
