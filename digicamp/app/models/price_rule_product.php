<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Price_rule_product extends Model{
	protected $table 	= 'price_rule_product';
	public $timestamps = false;
}
