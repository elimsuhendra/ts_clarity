<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product_data_attribute extends Model{
	protected $table 				= 'product_data_attribute';
	protected $product_attribute_data 	= 'digipos\models\Product_attribute_data';

	public function product_attribute_data(){
		return $this->belongsTo($this->product_attribute_data,'product_attribute_data_id');
	}
}
