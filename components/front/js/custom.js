 $(document).ready(function(){
        //Slideshow JS
        // $('.owl-carousel').owlCarousel({
        //     items:3,
        //     lazyLoad:true,
        //     loop:true,
        //     margin:5,
        //     autoplay:true,
        //     dots: false,
        //     smartSpeed :900,
        //     responsiveClass:true,
        //     autoplayHoverPause:true,
        //     animateOut: 'fadeOut',
        //     navigation : true,
        //     navText : ["<",">"],
        //     responsive:{
        //         0:{
        //             items:1
        //         },
        //         600:{
        //             items:3
        //         },
        //         1000:{
        //             items:3
        //         }
        //     }
        // });

        // navbar menu collapse
        $(document).click(function (event) {
            var clickover = $(event.target);
            var _opened = $(".navbar-collapse").hasClass("navbar-collapse collapse in");
            if (_opened === true && !clickover.hasClass("navbar-toggle")) {
                $("button.navbar-toggle").click();
            }
        });

        // search Head
        $("#button").click(function(){
            $("#head_search").slideToggle();    
        });

        $('#search_img').on({
        'click': function() { 
                var check = $(this).attr('src');
                var baseurl = $('meta[name="root_url"]').attr('content');
                var src = check.indexOf("search") != -1 ? baseurl + 'components/front/images/icon/click.png' : baseurl + 'components/front/images/icon/search.png';
                $(this).attr('src', src);
           }, 
           'mouseover':function(){
                var check = $(this).attr('src');
                var baseurl = $('meta[name="root_url"]').attr('content');
                if(check.indexOf("search") != -1) {
                    var src = baseurl + 'components/front/images/icon/search_hover.png';
                    $(this).attr('src', src);
                } else {
                    var src = baseurl + 'components/front/images/icon/click_hover.png';
                    $(this).attr('src', src);
                }
           },
           'mouseout':function(){
                var check = $(this).attr('src');
                var baseurl = $('meta[name="root_url"]').attr('content');
                if(check.indexOf("search") != -1) {
                    var src = baseurl + 'components/front/images/icon/search.png';
                    $(this).attr('src', src); 
                } else {
                    var src = baseurl + 'components/front/images/icon/click.png';
                    $(this).attr('src', src); 
                }
           }
        });
        // end search head

        // product hover
        $(".pro_hover").mouseenter(function(){
            var id = $(this).attr('id').split('_');
            $('#img_hover_'+ id[2]).show();
        });
        $(".pro_hover").mouseleave(function(){
            var id = $(this).attr('id').split('_');
            $('#img_hover_'+ id[2]).hide();
        });
        // End product hover

        // Header FIxed
        var header = $('.Fx_wrap');

        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();
               if (scroll >= 280) {
                  $('.fxd_con').addClass("fxd_wrap");
                  $('.fxd_con2').addClass("fxd_wrap2");
                  $('.C_NVfxd').addClass("C_nav");
                } else {
                  $('.fxd_con').removeClass("fxd_wrap");
                  $('.fxd_con2').removeClass("fxd_wrap2");
                  $('.C_NVfxd').removeClass("C_nav");
                }
        });
}); 

function search_function(id){
    var name = $('#s_'+id);

    var value = $(name).text();
    $('#hd_search').val(value);
    $('.rs_search').css("display", "none");
}

function fb_share(e){
	e.preventDefault();
    window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    return false;
}